# 分页组件　（Pagination）

```tsx
import { Pagination } from '@casstime/bricks';

<Pagination pageSize={10} total={100} onChange={this.toPage}>
```

### 一般使用
:::demo 一般使用
```tsx
class Demo extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props)
    this.toPage =  this.toPage.bind(this);
  }

  toPage(current: number, pageSize: number) {
    alert(`跳转至${current}页,每页${pageSize}条`);
  }

  render() {
    return (
      <Pagination
        pageSize={20}
        total={450}
        onChange={ this.toPage }
      />
    )
  }
}
```
:::

### 对齐方式
:::demo 右对齐
```tsx
class Demo extends React.Component<{}, {}> {
  render() {
    return (
      <Pagination
        pageSize={20}
        total={450}
        align="right"
        onChange={(current, pageSize) => alert(`跳转至${current}页,每页${pageSize}条`) }
      />
    )
  }
}
```
:::

### 默认页码
:::demo  默认当前页
```tsx
class Demo extends React.Component<{}, {}> {
  render() {
    return (
      <Pagination
        pageSize={20}
        defaultCurrent={5}
        total={450}
        onChange={(current, pageSize) => alert(`跳转至${current}页,每页${pageSize}条`) }
      />
    )
  }
}
```
:::


### 快速跳转
:::demo  显示快速跳转
```tsx
class Demo extends React.Component<{}, {}> {
  render() {
    return (
      <Pagination
        showQuickJumper
        pageSize={20}
        defaultCurrent={5}
        total={450}
        onChange={(current, pageSize) => alert(`跳转至${current}页,每页${pageSize}条`) }
      />
    )
  }
}
```
:::

### 更少条目
:::demo  更少条目
```tsx
class Demo extends React.Component<{}, {}> {
  render() {
    return (
      <Pagination
        showQuickJumper
        showLessItems
        pageSize={20}
        current={5}
        total={450}
        onChange={(current, pageSize) => alert(`跳转至${current}页,每页${pageSize}条`) }
      />
    )
  }
}
```
:::

## 参数描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| total | number | 0 | 条目总数 |
| pageSize | number | 10 | 每页条目数量 |
| defaultCurrent | number | 1 | 默认当前页面, current属性不存在时，使用defaultCurrent |
| current | number | undefined | 当前页面 |
| showQuickJumper | boolean | false | 是否显示快速跳转 |
| showLessItems | boolean | false | 是否显示更少的分页条目 |
| className | string | '' | 自定义class |
| style | React.CSSProperties | undefined | 自定义样式 |
| align | string | center | 'center' 、'left' 、 'right'，分别是居中，左对齐，右对齐 |
| onChange | (current: number, pageSize: number) => void | undefined | 页码改变的回调函数，第一个参数为改变后的页码，第二个参数为分页大小 |

分页组件需要传入 total 和 pageSize 属性， total 指的是条目数量，pageSize是每页数量
