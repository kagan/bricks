# Backdrop

```tsx
import { Backdrop } from '@casstime/bricks';

<Backdrop visible={this.state.visible} onBackdropClick={this.onBackdropClick} />
```

### 一般使用

:::demo
```tsx
interface IExampleState {
  visible: boolean;
}

class Demo extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = { visible: false }
    this.showModal = this.showModal.bind(this);
    this.onBackdropClick = this.onBackdropClick.bind(this);
  }

  showModal () {
    this.setState({
      visible: true,
    });
  }

  onBackdropClick () {
    this.setState({
      visible: false,
    });
  }

  render() {
    return (
      <div>
        <button className="button button-primary" onClick={this.showModal}>Open</button>
        <Backdrop visible={this.state.visible} onBackdropClick={this.onBackdropClick} />
      </div>
    )
  }
}
```
:::

## Props

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| visible | boolean | undefined | Backdrop是否可见 |
| onBackdropClick | function | undefined | 自定义点击事件 |