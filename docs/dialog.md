# 对话框组件   Dialog(已废弃，请使用Modal替代)

```tsx
  <Modal visible={this.state.visible} onClose={this.handleCancel}>
    <Modal.Header>Basic Modal<Modal.Header>
    <Modal.Content>
        <p>Some contents...</p>
        <p>Some contents...</p>
        <p>Some contents...</p>
    </Modal.Content>
    <Modal.footer>
      <Button onClick={this.handleCancel}>Close</Button>
      <Button onClick={this.handleOk}>Save changes</Button>
    </Modal.footer>
  </Modal>
```