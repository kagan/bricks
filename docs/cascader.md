# Cascader
```tsx
import { Cascader, ICascaderOption } from '@casstime/bricks';

<Cascader options={[{label: '广东', value: 'guangdong'}, {label: '北京', value: 'beijing'}]} onChange={this.onChange}/>
```

### 一般使用

:::demo 示例
``` tsx
class Example extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    console.log('选择的结果', value)
  }

  render() {
    return (
      <React.Fragment>
        <Cascader options={options} onChange={this.onChange}/>
        <Cascader options={options2} onChange={this.onChange} tabTitle={tabTitle} placeholder="请选择居住地址"/>
      </React.Fragment>
    );
  }
}

const tabTitle = ['省份', '城市', '县区'];

const options = [
  {
    name: '广东',
    id: 'guangdong',
    children: [
      {
        name: '深圳',
        id: 'shenzhen',
        children: [
          {
            name: '龙岗区',
            id: 'longgangqu',
            children: [
              {
                name: '坂田',
                id: 'bantian',
              }, 
              {
                name: '五和',
                id: 'wuhe',
              },
            ],
          }, 
          {
            name: '南山区',
            id: 'nanshanqu',
            children: [
              {
                name: '下沙',
                id: 'xiasha',
              }, 
              {
                name: '深南',
                id: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        name: '广州',
        id: 'guangzhou',
        children: [
          {
            name: '白云区',
            id: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
  {
    name: '北京',
    id: 'beijin',
  },
];

const options2 = [
  {
    name: '广东',
    id: 'guangdong',
    children: [
      {
        name: '深圳',
        id: 'shenzhen',
        children: [
          {
            name: '龙岗区',
            id: 'longgangqu',
          },
        ],
      }, 
      {
        name: '广州',
        id: 'guangzhou',
      },
    ],
  }, 
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    name: '北京',
    id: 'beijin',
  },
];

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 添加默认值的情况
:::demo 示例
``` tsx
class Example extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props);
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    console.log('选择的结果', value)
  }

  render() {
    return (
      <Cascader options={options} defaultValue={defaultValue} onChange={this.onChange}/>
    );
  }
}

const defaultValue = [
  {id: 'guangdong', name: '广东'},
  {id: 'shenzhen', name: '深圳'},
  {id: 'longgangqu', name: '龙岗区'},
  {id: 'bantian', name: '坂田'}
];

const options = [
  {
    name: '广东',
    id: 'guangdong',
    children: [
      {
        name: '深圳',
        id: 'shenzhen',
        children: [
          {
            name: '龙岗区',
            id: 'longgangqu',
            children: [
              {
                name: '坂田',
                id: 'bantian',
              }, 
              {
                name: '五和',
                id: 'wuhe',
              },
            ],
          }, 
          {
            name: '南山区',
            id: 'nanshanqu',
            children: [
              {
                name: '下沙',
                id: 'xiasha',
              }, 
              {
                name: '深南',
                id: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        name: '广州',
        id: 'guangzhou',
        children: [
          {
            name: '白云区',
            id: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    name: '北京',
    id: 'beijin',
  },
];

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 组件受控的情况
:::demo 示例
``` tsx
interface IExampleState {
  value: ICascaderOption[];
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      value: [],
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(value) {
    this.setValue(value)
  }

  setValue(value) {
    let info = [];
    value.forEach((v, i) => {
      info[i] = {
        id: v.id,
        name: v.name + '地区'
      };
    })
    this.setState({
      value: info,
    });
  }

  render() {
    const {value} = this.state;
    return (
      <Cascader options={options} value={value} onChange={this.onChange}/>
    );
  }
}

const options = [
  {
    name: '广东',
    id: 'guangdong',
    children: [
      {
        name: '深圳',
        id: 'shenzhen',
        children: [
          {
            name: '龙岗区',
            id: 'longgangqu',
            children: [
              {
                name: '坂田',
                id: 'bantian',
              }, 
              {
                name: '五和',
                id: 'wuhe',
              },
            ],
          }, 
          {
            name: '南山区',
            id: 'nanshanqu',
            children: [
              {
                name: '下沙',
                id: 'xiasha',
              }, 
              {
                name: '深南',
                id: 'shennan',
              },
            ],
          },
        ],
      }, 
      {
        name: '广州',
        id: 'guangzhou',
        children: [
          {
            name: '白云区',
            id: 'baiyunqu',
          },
        ],
      },
    ],
  }, 
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  }, 
  {
    name: '北京',
    id: 'beijin',
  },
];

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 选项异步加载
:::demo 示例
``` tsx
interface IExampleState {
  options: ICascaderOption[];
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      options: provinceOptions,
    }
    this.onChange = this.onChange.bind(this);
    this.loadData = this.loadData.bind(this);
  }

  onChange(value) {
    console.log('选择的结果', value)
  }

  loadData(targetOption) {
    const {options} = this.state;
    const that = this;
    let promise = new Promise(function(resolve, reject) {
      setTimeout(() => {
          targetOption.children = [
            {
              name: `${targetOption.name} 1`,
              id: `${targetOption.id}1`,
            }, 
            {
              name: `${targetOption.name} 2`,
              id: `${targetOption.id}2`,
            }
          ];
          that.setState({
            options: [...that.state.options],
          })
          resolve(targetOption);
      }, 100)
    });
    return promise;
  }

  render() {
    const {options, loaded} = this.state;
    return (
      <Cascader options={options} onChange={this.onChange} loadData={this.loadData} tabTitle={tabTitle} />
    );
  }
}

const tabTitle=['省份', '城市', '县区', '街道'];

const provinceOptions = [
  {
    name: '广东',
    id: 'guangdong',
  }, 
  {
    name: '浙江',
    id: 'Zhejiang',
    children: [
      {
        name: '杭州',
        id: 'Hangzhou',
        children: [
          {
            name: '西湖',
            id: 'West Lake',
          },
        ],
      },
    ],
  }, 
  {
    name: '江苏',
    id: 'Jiangsu',
    children: [
      {
        name: '南京',
        id: 'Nanjing',
        children: [
          {
            name: '中华门',
            id: 'Zhong Hua Men',
          },
        ],
      },
    ],
  },
];

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Cascader 组件 props 描述
| 属性 | 类型 | 默认值| 描述 |
| ---- | ----| ---- | ---- |
| className | string | '' | 自定义className |
| style | React.CSSProperties | {} | 自定义style |
| options | ICascaderOption[] | [] | 选项内容 | 选项内容 |
| value | ICascaderOption[] | [] | 传入则组件受控，显示传入的内容 |
| defaultValue | ICascaderOption[] | [] | 级联选择默认值 |
| tabTitle | string[] | ['省份', '城市', '县区', '街道'] | 级联层级tab自定义 |
| placeholder | string | 请选择收货地址 | 未填写时的placeholder |
| onChange | function: (value: ICascaderOption[]) => void | (value) => void | 选择完成后的回调 |
| loadData | function: (value: ICascaderOption) => Promise<ICascaderOption> | undefined | 动态加载选项（当前选中选项提前加载到options） |