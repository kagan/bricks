# 折叠组件 (Collapse)

```tsx
import { Collapse } from '@casstime/bricks';

<Collapse active>defaultConfig</Collapse>
```

### 一般使用
:::demo
```tsx
interface IExampleState {
  active: boolean;
  springConfig: string[];
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = { 
      active: true,
      springConfig: ["default", "wobbly", "stiff", "slow", "molasses"]
    }
     this.showCollapse = this.showCollapse.bind(this);
     this.hideCollapse = this.hideCollapse.bind(this);
     this.onStart = this.onStart.bind(this);
     this.onRest = this.onRest.bind(this);
  }

  showCollapse() {
    this.setState({  active: true });
  }

  hideCollapse() {
    this.setState({  active: false });
  }

  onStart() {
    console.log('start');
  }

  onRest() {
    console.log('destroyed');
  }

  render() {
    return (
      <div>
        <button className="button button-primary" onClick={this.state.active ? this.hideCollapse : this.showCollapse}>{this.state.active ? '收起' : '展开'}</button>
        <div className="container" style={{display: 'flex', flexDirection: 'row', marginTop: '20px'}}>
        {this.state.springConfig.map((item, index) => {
          return (
            <div style={{margin: '0 5px'}} key={index}>
              <h3 style={{fontSize: '20px'}}>{`${item}`}:</h3>
              <Collapse
                style={{
                  background: '#00c1aa',
                  color: '#fff',
                  padding: '20px',
                  fontSize: '20px',
                  lineHeight: '20px'
                 }} 
                 active={this.state.active} onStart={this.onStart} onRest={this.onRest} config={item}>
                <div>{`${item}`}</div>
              </Collapse>
            </div>
          );
        })}
      </div>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Collapse组件 Props 描述
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className |  string | '' | Collapse样式 |
| style |  React.CSSProperties  | undefined | Collapse样式 |
| active |  boolean | false | Collapse面板激活状态 |
| delay |  number | 0 | Collapse延时多久开始执行动画（单位：ms） |
| config |  string | 'default' | Collapse动画配置 （'default': 正常速度展开；'wobbly': 迅速展开；'stiff': 比正常速度慢展开;'slow': 比正常速度慢一点展开；'molasses': 比正常速度更慢一点展开；） |
| onStart |   function | undefined | Collapse动画开始执行时函数 |
| onRest | function | undefined | Collapse动画结束后执行函数 | 
