## 日期组件 DatePicker

```tsx
import { DatePicker } from '@casstime/bricks';
import moment, { Moment } from 'moment';

<DatePicker value={moment('2018-08-06')} />
```

### 简单使用
:::demo 示例
```tsx
interface IExampleState {
  value?: Moment;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      value: moment('2018-06-03 12:00:00'),
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(date: Moment) {
    console.log('dateChange: ', date);
    this.setState({ value: date}); 
  }

  render() {
    return (
      <Row>
        <Col md="6">
          <FormControl>
            <DatePicker
              onChange={this.onChange}
              value={this.state.value}
              placeholder="请选择时间"
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 简单使用-默认展开DatePicker
:::demo 示例
```tsx
interface IExampleState {
  value?: Moment;
  open?: boolean;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      value: moment('2018-06-03 12:00:00'),
      open: true
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(date: Moment) {
    console.log('dateChange: ', date);
    this.setState({ value: date}); 
  }

  render() {
    return (
      <Row>
        <Col md="6">
          <FormControl>
            <DatePicker
              open={this.state.open}
              onChange={this.onChange}
              value={this.state.value}
              placeholder="请选择时间"
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 日期范围选择
:::demo 示例
```tsx

interface IExampleState {
  startValue?: Moment | null;
  endValue?: Moment | null;
  endOpen?: boolean;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      startValue: null,
      endValue: null,
      endOpen: false,
    }
  }
  
  onChange(field: string, value: Moment){
    console.log('field', field);
    console.log('value', value);
    this.setState({
      [field]: value,
      endOpen: field == 'startValue'
    });
  }

  render() {
    return (
      <Row>
        <Col md={6}>
          <FormControl>
            <DatePicker
              onChange={this.onChange.bind(this, 'startValue')}
              value={this.state.startValue}
              placeholder="startTime"
            />
          </FormControl>
        </Col>
        <Col md={6}>
          <FormControl>
            <DatePicker
              open={this.state.endOpen}
              onChange={this.onChange.bind(this, 'endValue')}
              value={this.state.endValue}
              placeholder="endTime"
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不可选择日期
:::demo 示例
```tsx
interface IExampleState {
  startValue?: Moment | null;
  endValue?: Moment | null;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      startValue: null,
      endValue: null,
    }
    this.disabledStartDate = this.disabledStartDate.bind(this);
    this.disabledEndDate = this.disabledEndDate.bind(this);
  }
  
  onChange(field: string, value: Moment) {
    this.setState({
      [field]: value,
    });
  }

  disabledEndDate (endValue: Moment){
    if (!endValue) {
      return false;
    }
    const startValue = this.state.startValue;
    if (!startValue) {
      return false;
    }
    return endValue.isBefore(startValue);
  }

  disabledStartDate(startValue: Moment){
    if (!startValue) {
      return false;
    }
    const endValue = this.state.endValue;
    if (!endValue) {
      return false;
    }
    return endValue.isBefore(startValue);
  }

  render() {
    return (
      <Row>
        <Col md={6}>
          <FormControl>
            <DatePicker
              onChange={this.onChange.bind(this, 'startValue')}
              value={this.state.startValue}
              placeholder="startTime"
              disabledDate={this.disabledStartDate}
            />
          </FormControl>
        </Col>
        <Col md={6}>
          <FormControl>
            <DatePicker
              onChange={this.onChange.bind(this, 'endValue')}
              value={this.state.endValue}
              placeholder="endTime"
              disabledDate={this.disabledEndDate}
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 包含时间选择
:::demo 示例
```tsx
interface IExampleState {
  value: Moment;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      value: moment('2018-06-03 12:00:00'),
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(date: Moment) {
    console.log('dateChange: ', date);
    this.setState({ value: date});
  }

  render() {
    return (
      <Row>
        <Col md="6">
          <FormControl>
            <DatePicker
              showTime
              onChange={this.onChange}
              value={this.state.value}
              placeholder="请选择时间"
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不可选择时间
:::demo 示例
```tsx
interface IExampleState {
  value: Moment;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state={
      value: moment('2018-06-03 12:00:00'),
    }
    this.onChange = this.onChange.bind(this);
    this.disabledHours = this.disabledHours.bind(this);
    this.disabledMinutes = this.disabledMinutes.bind(this);
    this.disabledSeconds = this.disabledSeconds.bind(this);
  }

  onChange(date: Moment) {
    console.log('dateChange: ', date);
    this.setState({ value: date}); 
  }
  
  disabledHours() {
    return [0, 1, 2, 3, 4, 5, 6, 7, 8, 22, 23];
  }
  
  disabledMinutes(h: number) {  //根据小时去禁用分钟，直接返回则禁用全部小时包含在数组中的分钟
    switch (h) {
      case 9:
        return [1, 2, 3, 4, 5]; //禁用9点前5分钟
    }
  }
  
  disabledSeconds(h: number, m: number) { //根据小时、分钟去禁用秒，直接返回则禁用全部包含在数组中的秒
    return [h + m % 60];
  }

  render() {
    return (
      <Row>
        <Col md="6">
          <FormControl>
            <DatePicker
              showTime
              disabledHours={this.disabledHours}
              disabledMinutes={this.disabledMinutes}
              disabledSeconds={this.disabledSeconds}
              onChange={this.onChange}
              value={this.state.value}
              placeholder="请选择时间"
            />
          </FormControl>
        </Col>
      </Row>
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Props 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | '' | calendar样式 |
| value | moment | '' | 当前值，如输入值 |
| disabled | boolean | false | 禁用DatePicker |
| onChange | Function | '' | 选择不同的日期/时间时调用 |
| placeholder | string | '' | 占位符 |
| disabledDate | (current: Moment) => boolean | false | 是否禁用某些日期 |
| open | boolean | undefined | 是否默认展开 |
| showTime | boolean | false | 是否可选时间点 |
| showSecond | boolean | true | 是否显示秒数 |
| minuteStep | number | '' | 控制分钟时间间隔(可选) |
| disabledHours(在showTime下有效) | () => number[] | undefined | 禁用某些小时 |
| disabledMinutes(在showTime下有效) | (hour) => number[] | undefined | 禁用某些小时下的某些分钟 |
| disabledHours(在showTime下有效) | (hour, minute) => number[] | undefined | 是否禁用某些小时下的某些分钟的某些秒 |


`<DatePicker />` 组件基于 `rc-time-picker` 封装，更多功能请参考 [rc-time-picker](https://github.com/react-component/time-picker)