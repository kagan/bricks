## 日期组件 DateRangePicker

### 简单使用
:::demo 示例
```jsx
class Demo extends React.Component{
  constructor() {
    super();
    this.state={
      value: {
        value: 1,
        year: 2018,
      },
      type: 'week',
      open: true,
    }
    this.onChange = this.onChange.bind(this);
    this.handleRadioChange = this.handleRadioChange.bind(this);
    this.disabledYear = this.disabledYear.bind(this);
  }

  onChange(date, dateString) {
    let format = 'YYYY-MM-DD HH:mm:ss'; 
    console.log('dateChange: ', date);
    console.log(date.startTime.format(format));
    console.log(date.endTime.format(format));
    this.setState({ value: date}); // date 为 Moment 数组类型
  }
  
  disabledYear(year) {
    return year > 2000 && year < 2018;
  }
  
  handleRadioChange(e) {
    this.setState({
      type: e.target.value,
      open: false,
    });
  }

  render() {
    return (
      <div>
        <FormGroup>
          <Col md={6}>
          <FormControl>
            <div>
              <Radio 
                name="周"
                checked={this.state.type === 'week'}
                onChange={this.handleRadioChange}
                value="week"
              />
              <Radio 
                name="月"
                checked={this.state.type === 'month'}
                onChange={this.handleRadioChange}
                value="month"
              />
              <Radio 
                name="季度"
                checked={this.state.type === 'quarter'}
                onChange={this.handleRadioChange}
                value="quarter"
              />
              <Radio 
                name="半年"
                checked={this.state.type === 'half'}
                onChange={this.handleRadioChange}
                value="half"
              />
              <Radio 
                name="年"
                checked={this.state.type === 'year'}
                onChange={this.handleRadioChange}
                value="year"
              />
            </div>
          </FormControl>
          </Col>
        </FormGroup>
        <FormGroup>
          <Col md={4}>
            <FormControl>
              <DateRangePicker
                disabledYear={this.disabledYear}
                onChange={this.onChange}
                open={this.state.open}
                value={this.state.value}
                type={this.state.type}
                placeholder="请选择时间"/>
            </FormControl>
          </Col>
        </FormGroup>
      </div>
    );
  }
}
```
:::

### DateRangePicker暴露获取周、月、季度、半年度、年时间间隔接口
```jsx
import { DateRangePicker, moment } from '@casstime/bricks';

// 获取2018年第一周开始时间以及结束时间，返回两个字段startTime、endTime，两个字段均为moment类型，可直接使用moment.format()
let dateRange = DateRangePicker.getRange({
  type: 'week',
  value: 1,
  year: 2018,
})

dateRange.startTime.format('YYYY-MM-DD HH:mm:ss') => '2018-01-01 00:00:00'
dateRange.endTime.format('YYYY-MM-DD HH:mm:ss') => '2018-01-07 23:59:59'
```

## Props 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | '' | calendar样式 |
| style |  React.CSSProperties  | undefined | Collapse样式 |
| value | Object{}, 包含year、value两个字段 | undefined | year字段为年，设置全年value为-1；type为week时，value字段的值从1~53；type为month时，value字段的值从1~12；type为quarter时，value字段的值从1~4；type为half时，value字段的值从1~2；type为year时，value字段的值=year字段的值； |
| placeholder | string | '' | 占位符 |
| open | boolean | undefined | 是否默认展开 |
| type | string | 'week' | 值为'week', 'month', 'quarter', 'half', 'year'之一 |
| startYear | number | undefined | 设置组件的开始年份（主要为type为'year'时使用，其他请使用disabledYear） |
| endYear | number | undefined | 设置组件的结束年份（主要为type为'year'时使用，其他请使用disabledYear） |
| onChange | Function:({startTime: moment, endTime: moment}) => void | '' | 选择不同的日期/时间时调用, 返回数据是一个包含startTime以及endTime字段的对象，均为moment类型 |
| disabledYear | (year: number) => boolean | undefined | 是否禁用某些年份 |
