# 数字调整框组件 SpinBox

```tsx
import { SpinBox } from '@casstime/bricks';

<SpinBox onChange={this.handleChange}/>
```

### 一般使用 

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox onChange={this.handleChange}/>
    );
  }
}
```
:::

### 扩展使用

## 小型 SpinBox

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox small onChange={this.handleChange}/>
    );
  }
}
```
:::

## 设置初始默认值`5`

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox defaultValue={5} onChange={this.handleChange}/>
    );
  }
}
```
:::

## 不可编辑 SpinBox

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox editable={false} onChange={this.handleChange}/>
    );
  }
}
```
:::

## 禁用 SpinBox

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox disabled onChange={this.handleChange}/>
    );
  }
}
```
:::

## 设置最小值`0`,最大值`10`

:::demo
```tsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox minValue={0} maxValue={10} onChange={this.handleChange} />
    );
  }
}
```
:::

## 设置步长

:::demo
```jsx
class Demo extends React.Component<{}, {}>{
  constructor(props: {}) {
    super(props);
    this.handleChange = this.handleChange.bind(this);
  }
  handleChange(value: number) {
    console.log(value);
  }
  render() {
    return (
      <SpinBox minValue={0} maxValue={10} step={2} onChange={this.handleChange} />
    );
  }
}
```
:::

## SpinBox Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | String | '' | 自定义className |
| style | Object | null | 自定义样式 |
| editable | Boolean | true | 可编辑SpinBox |
| disabled | Boolean | false | 禁用SpinBox |
| small | Boolean | false | SpinBox的类型,默认大型30px,加上为小型 |
| onChange | Function(value: number) | undefined | onChange函数，参数为改变后的值 |
| minValue | Number | &nbsp; | SpinBox的最小值 |
| maxValue | Number | &nbsp; | SpinBox的最大值 |
| defaultValue | Number | 1 | SpinBox的默认值 |
| step | Boolean | 1 | SpinBox的步长 |
