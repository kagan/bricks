# 开发者文档

## 开发步骤(以开发REACT组件为例)：

### step1： 

​ 新建.tsx文件，编写组件逻辑（src\components）

### step2：

​	配置组件样式（src\styles）: 

​	新建.scss文件，或者配置外部导入的.less文件，并将写好的样式文件import至bricks.sass文件或brick.less文件中。

### step3：

​	将step1中编写好的组件配置到项目中（src\index.ts）

### step4：

​	添加该组件的描述文档.md（docs）

### step5：

​	把该组件相关文件配置到导航栏目录中（site\config\index.js）。配置完成之后可以通过bricks项目的导航栏目录进入该组件的.md文档界面，方便对该组件的文档进行查看，并且对该组件功能样式进行优化。



## 调试：

### 运行方式：

```bash
npm install

npm run build
```



 ### 调试：

```bash
npm run site
```

 ### 其他配置：
prop-types版本：
 ```bash
 npm install @types/prop-types@15.5.3 --save-dev
 ```
  


