## 面板 Panel
### 如何使用
通过传入header 和 主体content
- header 头部内容;
- 主体content：面板主体内容。

```tsx
import { Panel } from '@casstime/bricks';

<Panel header="Panel-header">panel-content</Panel>
```

### 一般使用
:::demo 示例
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Panel
        header={<div>header contents</div>}
      >
        <p>panel content</p>
      </Panel>
    );
  }
}
```
:::

### 嵌套使用
:::demo 示例
```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <Panel
        header={<div>主标题</div>}
      >
        <Panel
          header={<div>header</div>}
        >
          <p>content</p>
        </Panel>
      </Panel>
    );
  }
}
```
:::

## Option 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| header | React.ReactNode | '' | 标题内容(可选) |
| children | React.ReactNode | null | 主体内容 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
