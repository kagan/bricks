# 穿梭框

```tsx
import { Transfer, ITree } from '@casstime/bricks';

<Transfer treeData={treeData} onChange={this.handleChange} />
```

## 一般使用
:::demo 示例

```jsx
const treeData = [];
for(let i = 0; i < 5000; i ++) {
 treeData.push({ 
   key:  'key' + i, 
   title: '台湾' + i, 
   children: [
    { key: 'key-1-' + i, 
    title: 'sub-1-' + i ,
    children: [
    { key: 'key-1-1-' + i, title: 'sub-1-1-' + i }, 
    { key: 'key-2-1-' + i, title: 'sub-2-1-' + i }
    ]
    }, 
    { key: 'key-2-' + i, title: 'sub-2-' + i }
    ]
  });
}

class Demo extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = { treeData }; 
  }

  handleChange(tree) {
    console.log('状态变更后的tree', tree);
    this.setState({ treeData: tree });
  }

  render() {
    return (
      <div>
        <Transfer 
          treeData={this.state.treeData} 
          filterable
          filterPlaceholder="请搜索"
          resultBoxTitle="已选择结果"
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

```
:::

## 默认选中、展开

:::demo 示例
```jsx
const treeData = [];
for(let i = 0; i < 5000; i ++) {
 treeData.push({ 
   key:  'key' + i, 
   title: '台湾' + i, 
   expanded: true,
   children: [
    { key: 'key-1' + i, title: 'sub-1-' + i, checked: !(i % 1000) }, // 默认选中0,1000,2000...
    { key: 'key-2' + i, title: 'sub-2-' + i }
    ]
  });
}

class Demo extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = { treeData };
  }

  handleChange(tree) {
    console.log('状态变更后的tree', tree);
    this.setState({ treeData: tree });
  }

  render() {
    return (
      <div>
        <Transfer 
          treeData={this.state.treeData} 
          filterable
          filterPlaceholder="请搜索"
          resultBoxTitle="已选择结果"
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

```
:::

## 含有禁用状态的节点

:::demo 示例
```jsx
const treeData = [];
for(let i = 0; i < 5000; i ++) {
 treeData.push({ 
   key:  'key' + i, 
   title: '台湾' + i, 
   expanded: true,
   children: [
    { key: 'key-1' + i, title: 'sub-1-' + i, checked: !(i % 1000), disabled: !(i % 1000) }, // 默认选中0,1000,2000...
    { key: 'key-2' + i, title: 'sub-2-' + i }
    ]
  });
}

class Demo extends React.Component {
  constructor() {
    super();
    this.handleChange = this.handleChange.bind(this);
    this.state = { treeData };
  }

  handleChange(tree) {
    console.log('状态变更后的tree', tree);
    this.setState({ treeData: tree });
  }

  render() {
    return (
      <div>
        <Transfer 
          treeData={this.state.treeData} 
          filterable
          filterPlaceholder="请搜索"
          resultBoxTitle="已选择结果"
          onChange={this.handleChange}
        />
      </div>
    );
  }
}

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| treeData | Tree[] | undefined | (必填)树数据，如果不传的化使用defaultTreeData |
| onChange | Function(Tree[]) | undefined | 树的状态（展开、选中）变更时回调 |
| filterable | boolean | false | 是否显示搜索框 |
| filterPlaceholder | string | '' | 搜索框的占位文字 |
| resultBoxHeader | Element | '' | 自定义选中结果框的头部 |
| resultBoxTitle | string | '' | 选中结果框标题，如果不传，标题栏将不显示 |
| resultShowParent | boolean | false | 结果框是否只显示选中的父节点 |
| resultShowParentTitle | boolean | false | 结果框是否显示父节点的title路径 |
| style | object | undefined | 自定义外层样式 |
| className | string | '' | 自定义className |

## Tree数据结构

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| key | string | '' | (必填，保证唯一)选项的key |
| title | string | '' | (必填)选项的name |
| checked | boolean | false | 节点被选中 |
| disabled | boolean | false | 节点是否为禁用状态|
| expanded | boolean | false | 节点是否展开 |
| children | Tree[] | null | 子数据 |