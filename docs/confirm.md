# 弹出窗 Confirm

```tsx
import { Confirm } from '@casstime/bricks';
```

### 一般使用

:::demo
```tsx
interface IExampleState {
  visible: boolean;
  showShade?: boolean;
  topTitle?: string;
  tipIcon?: JSX.Element;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      visible: false,
      showShade: true,
      topTitle: "删除",
      tipIcon: <Icon type="question-circle" color="#ffc602" size={44} ></Icon>,
    }
    this.showModal = this.showModal.bind(this);
    this.onClose = this.onClose.bind(this);
    this.onOk = this.onOk.bind(this);
    this.onCancel = this.onCancel.bind(this);
  }

  showModal () {
    this.setState({
      visible: true,
    });
  }

  onClose() {
    this.setState({
      visible: false,
    });
  }

  onOk() {
    this.setState({
      visible: false,
    });
  }

  onCancel() {
    this.setState({
      visible: false,
    });
  }

  render() {
    return (
      <div>
        <button className="button button-primary" onClick={this.showModal}>Open</button>
        { this.state.visible && <Confirm
            showShade = {this.state.showShade}
            topTitle = {this.state.topTitle}
            onClose = {this.onClose}
            onOk = {this.onOk}
            onCancel = {this.onCancel}
            tipIcon = {this.state.tipIcon}
          >
            <Confirm.Header> 您确定要删除该商品? </Confirm.Header>
            <Confirm.Content> 您可以选择移到关注,或删除商品 </Confirm.Content>
          </Confirm>
        }
      </div>
    )
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::


## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
|  topTitle | string | '' | 弹出框主题 |
|  tipIcon | Element | '' | 提示图标 |
|  showShade | boolean | true | 是否显示遮罩层 |
|  onClose | Function | '' | 关闭按钮点击事件 |
|  onOk | Function | '' | 确认按钮点击事件 |
|  onCancel | Function | '' | 取消按钮点击事件 |
|  confirmText | string | '确定' | 确认按钮文案 |
|  cancelText | string | '取消' | 取消按钮文案 |