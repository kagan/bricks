# bricks前置声明

## 安装

```bash
npm install @casstime/bricks --save
```

## 使用

```jsx
import { Button } from '@casstime/bricks';

<Button>Hello,world!</Button>
```

:::demo
```jsx
render() {
  return <Button>bricks</Button>
}
```
:::

## 说明

组件库引用了bootstrap的布局和重置模块，会将所有标签的box-sizing设置为 `border-box`。
