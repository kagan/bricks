# 工具栏组件 Navbar（React）

:::demo 工具栏组件示例
```jsx
class Demo extends React.Component {
  render() {
    return (
      <Navbar description="码云">
        <Navbar.Item><a href="#">开源软件</a></Navbar.Item>
        <Navbar.Item><a href="#">企业版</a></Navbar.Item>
        <Navbar.Item><a href="#">高校版</a></Navbar.Item>
        <Navbar.Item active><a href="#">我的码云</a></Navbar.Item>
      </Navbar>
    );
  }
}
```
:::


## Navbar组件 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| iconClassName | string | '' | 图标的className |
| description   | string | '' | 导航栏标题描述 |
| children | React.ReactNode | '' | 子数据 |


## Navbar.Item 子组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| children | React.ReactNode | '' | 子数据 |
| active   | boolean | false | 激活当前选项 |