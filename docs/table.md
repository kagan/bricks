# 表格组件

## 开思表格UI规范
- `金额`与`数量`紧邻摆放
- 金额统一用`千分位`
- `索引`信息，`重要信息`靠前。`操作选项`靠右
- 标题中的单位用`( )`标记
- 列表中内容如为`定长(或相差不大)`，则为`居中`显示
- 列表中内容如为`不固定的中英文内容`，则`居左`显示
- 列表中的内容如为`数值`形式，则为`居右`显示

```tsx
import { Table, IColumnProps } from '@casstime/bricks';

<Table columns={this.state.columns} data={this.state.data} />
```

### 一般使用(推荐)

:::demo

```tsx
interface IDemoState {
  columns: IColumnProps[];
  data: {
    name?: string;
    age?: number;
    address?: string;
    key?: string;
  }[];
}
class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      columns: [{
        title: 'Name', dataIndex: 'name', key:'name', align:'center',width: 200,
      }, {
        title: 'Age', dataIndex: 'age', key:'age', align:'center',
      }, {
        title: 'Address', dataIndex: 'address', key:'address', align:'center',
      }, {
        title: 'Operations', dataIndex: '', key:'operations', align:'center', render: (value, row, index) => {
          console.log('value', value);
          console.log('每行数据row', row);
          console.log('index', index);
          return <a href="#">Delete</a>
        }, 
      }],
      data: [
        { name: '测试columns设置宽度就能自动换行', age: 28, address: 'some where', key:'1' },
        { name: 'Rose', age: 36, address: 'some where', key:'2' },
        { name: 'Jack', age: 28, address: 'some where', key:'3' },
        { name: 'Rose', age: 36, address: 'some where', key:'4' },
        { name: 'Jack', age: 28, address: 'some where', key:'5' },
        { name: 'Rose', age: 36, address: 'some where', key:'6' },
      ]
    }
  }

  render() {
    return (
      <Table columns={this.state.columns} data={this.state.data} />
    );
  }
}
```
:::

### 特殊使用(无竖线)

:::demo

```tsx
interface IDemoState {
  columns: IColumnProps[];
  data: {
    name?: string;
    age?: number;
    address?: string;
    key?: string;
  }[];
}
class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      columns: [{
        title: 'Name', dataIndex: 'name', key:'name', align:'center',
      }, {
        title: 'Age', dataIndex: 'age', key:'age', align:'center',
      }, {
        title: 'Address', dataIndex: 'address', key:'address', align:'center',
      }, {
        title: 'Operations', dataIndex: '', key:'operations', align:'center', render: (value, row, index) => {
          console.log('value', value);
          console.log('每行数据row', row);
          console.log('index', index);
          return <a href="#">Delete</a>
        }, 
      }],
      data: [
        { name: '测试columns设置宽度就能自动换行', age: 28, address: 'some where', key:'1' },
        { name: 'Rose', age: 36, address: 'some where', key:'2' },
        { name: 'Jack', age: 28, address: 'some where', key:'3' },
        { name: 'Rose', age: 36, address: 'some where', key:'4' },
        { name: 'Jack', age: 28, address: 'some where', key:'5' },
        { name: 'Rose', age: 36, address: 'some where', key:'6' },
      ]
    }
  }

  render() {
    return (
      <Table hideColumnLine={false} columns={this.state.columns} data={this.state.data} />
    );
  }
}

```
:::

## grouping columns (列分组)

:::demo

```tsx
interface IDemoStateColumn extends IColumnProps {
  children?: IDemoStateColumn[];
}
interface IDemoState {
  columns: IDemoStateColumn[];
  data: {
    name?: string;
    age?: number;
    address?: string;
    key?: string;
    align?: string;
    street?: string;
    building?: number;
    number?: number
    companyAddress?: string;
    companyName?: string;
    gender?: string;
  }[];
}

class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      columns: [
        {
          title: '姓名',
          dataIndex: 'name',
          key: 'name',
          align:'center',
          className:"grouping",
        },
        {
          title: '其它',
          className:"grouping",
          children: [
            {
              title: '年龄',
              dataIndex: 'age',
              key: 'age',
              align:'center',
              className:"grouping",
            },
            {
              title: '住址',
              className:"grouping",
              children: [
                {
                  title: '街道',
                  dataIndex: 'street',
                  key: 'street',
                  align:'center',
                  className:"grouping",
                },
                {
                  title: '小区',
                  className:"grouping",
                  children: [
                    {
                      title: '单元',
                      dataIndex: 'building',
                      key: 'building',
                      align:'center',
                      className:"grouping",
                    },
                    {
                      title: '门牌',
                      dataIndex: 'number',
                      key: 'number',
                      align:'center',
                      className:"grouping",
                    },
                  ],
                },
              ],
            },
          ],
        },
        {
          title: '公司',
          className:"grouping",
          children: [
            {
              title: '地址',
              dataIndex: 'companyAddress',
              key: 'companyAddress',
              align:'center',
              className:"grouping",
            },
            {
              title: '名称',
              dataIndex: 'companyName',
              key: 'companyName',
              align:'center',
              className:"grouping",
            },
          ],
        },
        {
          title: '性别',
          dataIndex: 'gender',
          key: 'gender',
          align:'center',
          className:"grouping",
        },
      ],
      data: [
        {
          key: '1',
          align:'center',
          name: '胡彦斌',
          age: 32,
          street: '拱墅区和睦街道',
          building: 1,
          number: 2033,
          companyAddress: '西湖区湖底公园',
          companyName: '湖底有限公司',
          gender: '男',
        },
        {
          key: '2',
          align:'center',
          name: '胡彦祖',
          age: 42,
          street: '拱墅区和睦街道',
          building: 3,
          number: 2035,
          companyAddress: '西湖区湖底公园',
          companyName: '湖底有限公司',
          gender: '男',
        },
      ]
    }
  }

  render() {
    return (
      <Table columns={this.state.columns} data={this.state.data} className="bordered" />
    );
  }
}

```
:::

## no-data (无数据显示)

:::demo

```tsx
interface IDemoState {
  columns: IColumnProps[];
  data?: [];
}
class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      columns: [
        { title: 'title1', dataIndex: 'a', key: 'a', width: 100,  },
        { title: 'title2', dataIndex: 'b', key: 'b', width: 100,  },
        { title: 'title3', dataIndex: 'c', key: 'c', width: 200,  },
        {
          title: 'Operations',
          dataIndex: '',
          key: 'd',
          render: (value, row, index) => {
            console.log('value', value);
            console.log('每行数据row', row);
            console.log('index', index);
            return <a href="#">Delete</a>
          }, 
        },
      ],
      data: []
    }
  }

  render() {
    return (
      <Table emptyText="没有数据" columns={this.state.columns} data={this.state.data} />
    );
  }
}

```
:::

## title and footer (表头和表底)

:::demo

```tsx
interface IDemoState {
  columns: IColumnProps[];
  data: {
    a?: string;
    b?: string;
    c?: string;
    d?: number;
    key?: string;
  }[];
}
class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      columns: [
        { title: 'title1', dataIndex: 'a', key: 'a', width: 100, align:'center', },
        { title: 'title2', dataIndex: 'b', key: 'b', width: 100, align:'center', },
        { title: 'title3', dataIndex: 'c', key: 'c', width: 200, align:'center', },
        {
          title: 'Operations',
          dataIndex: '',
          key: 'd',
          align:'center',
          render: (value, row, index) => {
            console.log('value', value);
            console.log('每行数据row', row);
            console.log('index', index);
            return <a href="#">Delete</a>
          }, 
        },
      ],
      data: [
        { a: '123', key: '1' },
        { a: 'cdd', b: 'edd', key: '2' },
        { a: '1333', c: 'eee', d: 2, key: '3' },
      ]
    }
  }

  render() {
    return (
      <Table columns={this.state.columns} data={this.state.data} 
        title={currentData => <div>Title: {currentData.length} items</div>}
        footer={currentData => <div>Footer: {currentData.length} items</div>}
      />
    );
  }
}

```
:::

## Table组件 Props 描述 (常用)

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| showVerticalBorder| Boolean | false | 是否隐藏列的竖线，一般默认隐藏。如需竖线改为false |
| columns | Object[] | &nbsp; | 表的列配置，参见下面的表格 |
| data | Object[] | &nbsp; | 要呈现的数据记录数组 |
| className | String | &nbsp; | 额外的类名 |
| id | String | &nbsp; | 容器div的标识符 |
| title | Function(currentData) | &nbsp; | 表标题渲染函数 |
| footer | Function(currentData) | &nbsp; | 表页脚显示函数 |
| emptyText | React.Node or Function | '暂无数据' | 数据为空时显示文本 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |

## Column Props 描述 (常用)

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| key | String | &nbsp; | 此列的key，类似id |
| colSpan | Number | &nbsp; | 此列的头跨距 |
| title | React.Node | &nbsp; | 此列的名字 |
| width | String or Number | &nbsp; | 根据列的宽度计算特定比例的宽度 |
| align | String | &nbsp; | 指定内容是如何对齐的 |
| render | Function(value, row, index) | &nbsp; | 单元格的render函数，有三个参数：这个单元格的文本，这一行的数据，这一行的索引，它返回一个对象:{ children: value, props: { colSpan: 1, rowSpan:1 } } ， 'children'是这个单元格的文本，'props'是这个单元格的一些设置，例如：“colspan”设置td colspan，“rowspan”设置td rowspan |

`<Table />` 组件基于 `rc-table` 封装，更多功能请参考 [rc-table](https://github.com/react-component/table)