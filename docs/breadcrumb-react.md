# 面包屑导航组件 Breadcrumb（React）

```tsx
import { Breadcrumb } from '@casstime/bricks';

<Breadcrumb description="您当前的位置是：">
  <Breadcrumb.Item><a href="#/">首页</a></Breadcrumb.Item>
  <Breadcrumb.Item><a href="#">会员中心</a></Breadcrumb.Item>
</Breadcrumb>
```

:::demo 普通面包屑导航组件示例
```tsx
interface IExampleState {
  icon?: string;
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      icon: "car",
    }
  }
  render() {
    return (
      <Breadcrumb description="您当前的位置是：" icon={this.state.icon}>
        <Breadcrumb.Item><a href="#/">首页</a></Breadcrumb.Item>
        <Breadcrumb.Item><a href="#">会员中心</a></Breadcrumb.Item>
        <Breadcrumb.Item><a href="#">财务中心</a></Breadcrumb.Item>
        <Breadcrumb.Item active>我的积分</Breadcrumb.Item>
      </Breadcrumb>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

:::demo 带边框的面包屑导航组件示例
```tsx
interface IExampleState {
  icon?: string;
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      icon: "car",
    }
  }
  handleClick() {
    alert('tag click');
  }
  render() {
    return (
      <Breadcrumb description="您当前的位置是：" icon={this.state.icon}>
        <Breadcrumb.Item><a href="#/">首页</a></Breadcrumb.Item>
        <Breadcrumb.Item><a href="#">列表页</a></Breadcrumb.Item>
        <Breadcrumb.Item><Tag onClick={this.handleClick}>机油</Tag></Breadcrumb.Item>
        <Breadcrumb.Item><Tag>全部</Tag></Breadcrumb.Item>
        <Breadcrumb.Item><Tag>机油</Tag></Breadcrumb.Item>
        <Breadcrumb.Item><Tag><a href="#">国外品牌</a></Tag></Breadcrumb.Item>
      </Breadcrumb>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::



## Breadcrumb组件 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| type | string | '' | 图标名称 |
| description   | string | '' | 导航栏标题描述 |
| children | React.ReactNode | '' | 子数据 |


## Breadcrumb.Item 子组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| children | React.ReactNode | '' | 子数据 |
| active   | boolean | false | 激活当前选项 |