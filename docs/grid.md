# 栅格布局

```tsx
import { Grid, Row, Col } from '@casstime/bricks';

<Grid fluid>
  <Row style={{ color: 'white' }}>
    <Col style={{ background: 'red' }} xs={12} md={6}>xs=12, md=6</Col>
    <Col style={{ background: 'black' }} xs={12} md={6}>xs=12, md=6</Col>
  </Row>
</Grid>
```

栅格布局使用的是 bootstrap 3 的栅格布局。因为留白有所差异，所以本项目中Col和Row组件将默认margin/padding设置为0,
更多请参考 [bootstrap grid](https://v3.bootcss.com/css/#grid)

## 示例

缩放窗口试试

:::demo Grid示例
```tsx
class Example extends React.Component<{}, {}> {
  render() {
    return (
      <Grid fluid>
        <Row style={{ color: 'white' }}>
          <Col style={{ background: 'red' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
          <Col style={{ background: 'blue' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
          <Col style={{ background: 'black' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
          <Col style={{ background: 'orange' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
          <Col style={{ background: 'pink' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
          <Col style={{ background: 'green' }} xs={6} md={3} lg={2}>xs=6, md=3, lg=2</Col>
        </Row>
      </Grid>
    )
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

## Grid 组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| fluid | boolean | false | 如果为false，生成的组件class为`container`,否则为`container-fluid` | 
| style | object | null | 自定义样式 | 
| className | string | '' | 自定义className | 

## Row 组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| style | object | null | 自定义样式 | 
| className | string | '' | 自定义className | 

同`<div class="row"></div>`

## Col 组件

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| xs | number | null | col-xs- |
| sm | number | null | col-sm- |
| md | number | null | col-md- |
| lg | number | null | col-lg- |
| xsOffset | number| null | col-xs-offset- |
| smOffset | number| null | col-sm-offset- |
| mdOffset | number| null | col-md-offset- |
| lgOffset | number| null | col-lg-offset- |
| xsPull | number | null | col-xs-push- |
| smPull | number | null | col-sm-push- |
| mdPull | number | null | col-md-push- |
| lgPull | number | null | col-lg-push- |
| xsPush | number | null | col-xs-push- |
| smPush | number | null | col-sm-push- |
| mdPush | number | null | col-md-push- |
| lgPush | number | null | col-lg-push- |
| xsHidden | boolean | false | hidden-xs |
| smHidden | boolean | false | hidden-sm |
| mdHidden | boolean | false | hidden-md |
| lgHidden | boolean | false | hidden-lg |
| style | object | null | 自定义样式 | 
| className | string | '' | 自定义className | 
