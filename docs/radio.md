# 单选按钮

```tsx
import { Radio } from '@casstime/bricks';

<Radio name="Radio" checked onChange={this.handleChange} value="Radio-value" />
```

### 单个按钮
:::demo 示例

```tsx
interface IDemoState {
  checkedValue: string;
}
class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      checkedValue: '',
    };
    
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e: React.ChangeEvent<HTMLInputElement>){
    this.setState({
      checkedValue: e.target.value,
    });
    console.log(e.target.value);
  }

  render() {
    return (
      <div>
        <Radio 
          name="鱼"
          checked={this.state.checkedValue === '123'}
          onChange={this.handleChange}
          value="123"
        />
        <Radio 
          name="熊掌"
          checked={this.state.checkedValue === '456'}
          onChange={this.handleChange}
          value="456"
        />
      </div>
    );
  }
}

```
:::

### 扩展使用
### 多个按钮
:::demo 示例

```jsx
class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedValue: 'one',
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e){
    this.setState({
      checkedValue: e.target.value,
    });
  }

  render() {
    return (
      <div>
        <Radio 
          name="按钮一"
          checked={this.state.checkedValue === 'one'}
          onChange={this.handleChange}
          value="one"
        />
        <Radio 
          name="按钮二"
          checked={this.state.checkedValue === 'two'}
          onChange={this.handleChange}
          value="two"
        />
        <Radio 
          name="按钮三"
          checked={this.state.checkedValue === 'three'}
          onChange={this.handleChange}
          value="three"
        />
        <Radio 
          name="按钮四"
          checked={this.state.checkedValue === 'four'}
          onChange={this.handleChange}
          value="four"
        />
      </div>
    );
  }
}

```
:::

### 禁用按钮
:::demo 示例

```jsx
class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      checkedValue: '',
      disbaled: true
    };
    this.handleChange = this.handleChange.bind(this);
  }

  handleChange(e){
    this.setState({
      checkedValue: e.target.value,
    });
  }

  render() {
    return (
      <div>
        <Radio 
          name="禁用"
          checked={this.state.checkedValue === 'disable'}
          onChange={this.handleChange}
          value="disable"
          disabled={this.state.disbaled}
        />
        <Radio 
          name="可用"
          checked={this.state.checkedValue === 'available'}
          onChange={this.handleChange}
          value="available"
        />
      </div>
    );
  }
}

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| value | string | 'on' | 单选按钮的唯一标识 |
| checked | boolean | false | 指定当前是否选中 |
| onChange | Function(event: React.ChangeEvent<HTMLInputElement>) | undefined | 变化时回调函数 |
| name | string | undefined | 文字标题 |
| disabled | boolean | false | 禁止使用 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |