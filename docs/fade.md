# fade淡入淡出组件

```tsx
import { Fade } from '@casstime/bricks';

<Fade active>Fade-content</Fade>
```

### 一般使用：在文档流中淡入淡出
:::demo
``` tsx
interface IDemoState {
  active: boolean;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      active: false
    }

    this.showFade = this.showFade.bind(this);
     this.hideFade = this.hideFade.bind(this);
  }

  showFade() {
    this.setState({  active: true });
  }

  hideFade() {
    this.setState({  active: false });
  }

  render() {
    return (
      <div>
        <button className="button button-primary"  onClick={this.state.active ? this.hideFade : this.showFade}>{this.state.active ? 'fade out' : 'fade in'}</button>
        <Fade active={this.state.active}>
          <div style={{
            padding: '20px',
            lineHeight: '20px',
            fontSize: '20px',
            background: '#00c1aa',
            color: '#fff'
          }}>这是一个淡入淡出组件</div>
        </Fade>
      </div>
    )
  }
}

```
:::

### 脱离文档流淡入淡出
:::demo
``` tsx
interface IDemoState {
  active: boolean;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);

    this.state = {
      active: false
    }
    this.showFade = this.showFade.bind(this);
     this.hideFade = this.hideFade.bind(this);
  }

  showFade() {
    this.setState({  active: true });
  }

  hideFade() {
    this.setState({  active: false });
  }

  render() {
    return (
      <div>
        <button className="button button-primary"  onClick={this.state.active ? this.hideFade : this.showFade}>{this.state.active ? 'fade out' : 'fade in'}</button>
        <Fade 
          active={this.state.active} 
          style={{position: 'fixed', top: 0, left: 0, width: '100%', height: '100%', background: 'rgba(0, 0, 0, .5)', zIndex: 9999}}
          onClick={this.hideFade}
        >
          <div>
            <div style={{
                padding: '20px',
                lineHeight: '20px',
                fontSize: '20px',
                background: '#00c1aa',
                color: '#fff'
              }}
            >
              这是一个淡入淡出组件
            </div>
          </div>
        </Fade>
      </div>
    )
  }
}
```
:::

## Fade组件 Props 描述
### ✔注意：在from、enter、leave中设置top或者left，元素需使用position定位！
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| active |  boolean | false | 组件是否淡入 |
| from | React.CSSProperties | undefined | 淡入初始样式(设置值只包含transform, width, height, opacity, top, left, index) |
| enter | React.CSSProperties | undefined | 淡入后样式(设置值只包含transform, width, height, opacity, top, left, index) |
| leave | React.CSSProperties | undefined | 淡出初始样式(设置值只包含transform, width, height, opacity, top, left, index) |
| delay |  number | 0 | Fade延时多久开始执行动画（单位：ms） |
| config |  string | 'default' | Fade动画配置(详情请参考Collapse组件) （'default': 正常速度展开；'wobbly': 迅速展开；'stiff': 比正常速度慢展开;'slow': 比正常速度慢一点展开；'molasses': 比正常速度更慢一点展开；） |