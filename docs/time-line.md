# 时间轴

```tsx
import { Timeline } from '@casstime/bricks';

<Timeline>
  <Timeline.Item active>
    <Row>
      <Col xs={4}>{name}</Col>
      <Col xs={4}>{event}</Col>
      <Col xs={4}>{time}</Col>
    </Row>
  </Timeline.Item>
</Timeline>
```

### react 组件使用
:::demo 示例

```tsx
interface IDemoState {
  arr: {
    active?: boolean;
    name?: string;
    event?: string;
    time?: string;
    key?: number;
  }[]
}
class Demo extends React.Component<{}, IDemoState> {

  constructor(props: {}) {
    super(props);
    this.state = {
      arr: [
        {name: '小明', event: 'event', time: '2018-08-01', key: 1},
        {name: '小华', event: 'event', time: '2018-08-02', key: 2},
        {name: '小李', event: 'event', time: '2018-08-03', key: 3},
      ]
    }
  }

  render() {
    const arr=this.state.arr;
    return (
      <Timeline>
           <Timeline.Item key={arr[0].key} active >
              <Row>
                <Col xs={4}>{arr[0].name}</Col>
                <Col xs={4}>{arr[0].event}</Col>
                <Col xs={4}>{arr[0].time}</Col>
              </Row>
            </Timeline.Item>
            <Timeline.Item key={arr[1].key}>
              <Row>
                <Col xs={4}>{arr[1].name}</Col>
                <Col xs={4}>{arr[1].event}</Col>
                <Col xs={4}>{arr[1].time}</Col>
              </Row>
            </Timeline.Item>
            <Timeline.Item  key={arr[2].key}>
              <Row>
                <Col xs={4}>{arr[2].name}</Col>
                <Col xs={4}>{arr[2].event}</Col>
                <Col xs={4}>{arr[2].time}</Col>
              </Row>
            </Timeline.Item>
      </Timeline>
    );
  }
}

```
:::

## Timeline组件 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | - | 自定义className |
| style | object | - | 自定义样式 |


## Timeline.Item 子组件 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | - | 自定义className |
| style | object | - | 自定义样式 |
| active | boolean | - | 是否为激活状态 |