# Menu
```tsx
import { Menu } from '@casstime/bricks';

<Menu>
  <Menu.Item name="index" active={ activeItem === 'index' } onClick={this.onMenuItemClick} >首页</Menu.Item>
  <Menu.Item name="member-center" active={ activeItem === 'member-center' } onClick={this.onMenuItemClick} >会员中心</Menu.Item>
  <Menu.Item name="finance-center" active={ activeItem === 'finance-center' } onClick={this.onMenuItemClick} >财务中心</Menu.Item>
  <Menu.SubMenu title="订单中心">
    <Menu.Item name="order-express" active={ activeItem === 'order-express' } onClick={this.onMenuItemClick} >订单物流</Menu.Item>
    <Menu.SubMenu title="订单中心子菜单">
    <Menu.Item name="children1" active={ activeItem === 'children1' } onClick={this.onMenuItemClick} >子菜单1</Menu.Item>
    <Menu.Item name="children2" active={ activeItem === 'children2' } onClick={this.onMenuItemClick} >子菜单2</Menu.Item>
  </Menu.SubMenu>
  </Menu.SubMenu>
  <Menu.Item name="points" active={ activeItem === 'points' } onClick={this.onMenuItemClick} disabled>我的积分</Menu.Item>
</Menu>
```

### 一般使用
hover出现
:::demo
```tsx
interface IDemoState {
  activeItem: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);

    this.state = { activeItem: 'index' }

    this.onMenuItemClick = this.onMenuItemClick.bind(this);
  }

  onMenuItemClick (name: string) {
    this.setState({
      activeItem: name,
    });
  }

  render() {
    const { activeItem } = this.state;
    return (
      <div>
        <Menu>
          <Menu.Item name="index" active={ activeItem === 'index' } onClick={this.onMenuItemClick} >首页</Menu.Item>
          <Menu.Item name="member-center" active={ activeItem === 'member-center' } onClick={this.onMenuItemClick} >会员中心</Menu.Item>
          <Menu.Item name="finance-center" active={ activeItem === 'finance-center' } onClick={this.onMenuItemClick} >财务中心</Menu.Item>
          <Menu.SubMenu title="订单中心">
            <Menu.Item name="order-express" active={ activeItem === 'order-express' } onClick={this.onMenuItemClick} >订单物流</Menu.Item>
            <Menu.SubMenu title="订单中心子菜单">
            <Menu.Item name="children1" active={ activeItem === 'children1' } onClick={this.onMenuItemClick} >子菜单1</Menu.Item>
            <Menu.Item name="children2" active={ activeItem === 'children2' } onClick={this.onMenuItemClick} >子菜单2</Menu.Item>
          </Menu.SubMenu>
          </Menu.SubMenu>
          <Menu.Item name="points" active={ activeItem === 'points' } onClick={this.onMenuItemClick} disabled>我的积分</Menu.Item>
        </Menu>
      </div>
    )
  }
}

```
:::

click出现
:::demo
```tsx
interface IDemoState {
  activeItem: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);

    this.state = { activeItem: 'index' }

    this.onMenuItemClick = this.onMenuItemClick.bind(this);
  }

  onMenuItemClick (name: string) {
    this.setState({
      activeItem: name,
    });
  }

  render() {
    const { activeItem } = this.state;
    return (
      <div>
        <Menu>
          <Menu.Item name="index" active={ activeItem === 'index' } onClick={this.onMenuItemClick} ><Icon type="search" size={14}/>首页</Menu.Item>
          <Menu.Item name="member-center" active={ activeItem === 'member-center' } onClick={this.onMenuItemClick} >会员中心</Menu.Item>
          <Menu.Item name="finance-center" active={ activeItem === 'finance-center' } onClick={this.onMenuItemClick} >财务中心</Menu.Item>
            <Menu.SubMenu title="订单中心"  outter triggerEvent="click" icon={<Icon type="search" size={14}/>}>
              <Menu.Item name="order-express" active={ activeItem === 'order-express' } onClick={this.onMenuItemClick} >订单物流</Menu.Item>
                <Menu.SubMenu title="订单中心子菜单" outter triggerEvent="click" icon={<Icon type="search" size={14}/>}>
                  <Menu.Item name="children1" active={ activeItem === 'children1' } onClick={this.onMenuItemClick} >子菜单1</Menu.Item>
                  <Menu.Item name="children2" active={ activeItem === 'children2' } onClick={this.onMenuItemClick} >子菜单2</Menu.Item>
              </Menu.SubMenu>
            </Menu.SubMenu>
          <Menu.Item name="points" active={ activeItem === 'points' } onClick={this.onMenuItemClick} disabled>我的积分</Menu.Item>
        </Menu>
      </div>
    )
  }
}
```
:::

### 竖方向Menu
hover出现
:::demo
```tsx
interface IDemoState {
  activeItem: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = { activeItem: 'index' }

    this.onMenuItemClick = this.onMenuItemClick.bind(this);
  }

  onMenuItemClick (name: string) {
    this.setState({
      activeItem: name,
    });
  }

  render() {
    const { activeItem } = this.state;
    return (
      <div>
        <Menu vertical style={{width: '150px'}}>
          <Menu.Item name="index" active={ activeItem === 'index' } onClick={this.onMenuItemClick} >首页</Menu.Item>
          <Menu.Item name="member-center" active={ activeItem === 'member-center' } onClick={this.onMenuItemClick} >会员中心</Menu.Item>
          <Menu.Item name="finance-center" active={ activeItem === 'finance-center' } onClick={this.onMenuItemClick} >财务中心</Menu.Item>
          <Menu.SubMenu title="订单中心" icon={<Icon type="search" size={14}/>}>
            <Menu.Item name="order-express" active={ activeItem === 'order-express' } onClick={this.onMenuItemClick} >订单物流</Menu.Item>
            <Menu.SubMenu title="订单中心2" icon={<Icon type="search" size={14}/>}>
              <Menu.Item name="order-express2" active={ activeItem === 'order-express2' } onClick={this.onMenuItemClick} >订单物流2</Menu.Item>
            </Menu.SubMenu>
          </Menu.SubMenu>
          <Menu.Item name="points" active={ activeItem === 'points' } onClick={this.onMenuItemClick} disabled>我的积分</Menu.Item>
        </Menu>
      </div>
    )
  }
}

```
:::

click出现
:::demo
```tsx
interface IDemoState {
  activeItem: string;
}

class Demo extends React.Component<{}, IDemoState> {
  constructor(props: {}) {
    super(props);
    this.state = { activeItem: 'index' }

    this.onMenuItemClick = this.onMenuItemClick.bind(this);
  }

  onMenuItemClick (name: string) {
    this.setState({
      activeItem: name,
    });
  }

  render() {
    const { activeItem } = this.state;
    return (
      <div>
        <Menu vertical style={{width: '150px'}}>
          <Menu.Item name="index" active={ activeItem === 'index' } onClick={this.onMenuItemClick} ><Icon type="search" size={14}/>首页</Menu.Item>
          <Menu.Item name="member-center" active={ activeItem === 'member-center' } onClick={this.onMenuItemClick} >会员中心</Menu.Item>
          <Menu.Item name="finance-center" active={ activeItem === 'finance-center' } onClick={this.onMenuItemClick} >财务中心</Menu.Item>
          <Menu.SubMenu title="订单中心" outter triggerEvent="click" icon={<Icon type="search" size={14}/>}>
            <Menu.Item name="order-express" active={ activeItem === 'order-express' } onClick={this.onMenuItemClick} >订单物流</Menu.Item>
            <Menu.SubMenu title="订单中心2" outter triggerEvent="click" icon={<Icon type="search" size={14}/>}>
              <Menu.Item name="order-express2" active={ activeItem === 'order-express2' } onClick={this.onMenuItemClick} >订单物流2</Menu.Item>
            </Menu.SubMenu>
          </Menu.SubMenu>
          <Menu.Item name="points" active={ activeItem === 'points' } onClick={this.onMenuItemClick} disabled>我的积分</Menu.Item>
        </Menu>
      </div>
    )
  }
}

```
:::

## Menu组件props描述
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| vertical | boolean | undefined | 菜单竖方向排列 |

## SubMenu组件props描述
| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| title | string | '' | 二级菜单标题 |
| icon | React.ReactNode | undefined | 二级菜单的左边icon |
| outter | boolean | false | submenu子元素在里面还是在外右边 |
| triggerEvent | string | 'hover'/'click' | 二级菜单的展开方式，点击还是hover |


## MenuItem组件props描述
菜单项分为大、中（默认）、小三种类型。
菜单项可直接插入icon

| 属性 | 类型 | 默认值| 描述 | 
| ---- | ----| ---- | ---- | 
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| large | Boolean | undefined |大菜单 |
| small | Boolean | undefined|小菜单 |
| name | string | undefined | 菜单name |
| active | boolean | false | 菜单激活状态 |
| onClick | function | (name?: string) => void; | 菜单点击事件 |
