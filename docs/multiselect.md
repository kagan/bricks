# Multiselect 多选下拉选择框

```tsx
import { MultiSelect, IOption } from '@casstime/bricks';
<MultiSelect name="select-name" options={options}/>
```

## 多选下拉选择框示例

:::demo
```tsx
interface IOptionsObject{
  value?: string;
  label?: string;
  disabled?: boolean;
}
interface IExampleState{
  options: IOptionsObject[];
  value: IOptionsObject[];
}
class Example extends React.Component<{}, IExampleState>{
  constructor(props: {}) {
    super(props);
    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' , disabled: false },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' , disabled: false },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' , disabled: false },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' , disabled: false },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' , disabled: false },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' , disabled: true },
      ],

     value:
        [{ value: 'A:安捷物流(汽)',label: 'A:安捷物流(汽)'}],
     };
    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onChange(option: IOptionsObject[]) {
    console.log('option', option);
    this.setState({ value: option });
  }

  onFocus() {
    console.log('onFocusonFocus');
  }

  render() {
    return (
      <MultiSelect
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
        onFocus={this.onFocus}
      />
    );
  }
}
ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))

```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| name | string | '' | 表单的name字段 |
| value | IOption[] | [] | 表单当前选项 |
| options | Option[] | null | 必填，下拉菜单数据 |
| onChange | Function(Option) | null | 选中后的回调 |
| onFocus | Function(Event) | null | 获得焦点时回调 |
| large | boolean | false | 大型下拉框 |
| small | boolean | false | 小型下拉框 |
| disabled | boolean | false | 设置下拉框不可用,可以直接在组件加该属性,无需赋值 |

### Option 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| label | string | '' | 选项显示文本 |
| value | string | '' | 选项的值 |
| disabled | boolean | '' | 选项是否可选 |

