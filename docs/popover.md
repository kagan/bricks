# popover气泡组件

```tsx
import { Popover } from '@casstime/bricks';

<Popover open={true} target={<div>target-node</div>}>
  <p>Popover-content</p>
</Popover>
```

### 一般使用

:::demo Popover示例
```tsx
class Demo extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      target: undefined,
      open: false,
    };
  }

  setTarget(event) {
    this.setState({
      target: event.target,
      open: !this.state.open,
    })
  }

  onChange(checked: boolean){
    console.log('checked', checked);
  }

  render() {
    return (
      <div className="button-demo">
        <button id='button' className='button' onMouseEnter={this.setTarget.bind(this)}  onMouseLeave={this.setTarget.bind(this)}>{this.state.open ? '鼠标leave关闭' : '鼠标hover显示'}popover</button>
   
        <Popover open={this.state.open} target={this.state.target}>
          <p>Lorem ipsum dolor sit amet, consectetuer adipiscing elit.<br />
           Aenean commodo ligula eget dolor. Aenean massa.<br />
           Cum sociis natoque penatibus et magnis dis parturient montes...</p>
        </Popover>
      </div>
    );
  }
}
```
:::


### placement位置示例
:::demo Popover  placement示例
```tsx
class Demo extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      placement: 'bottom',
      open: false,
      target: undefined,
    };

    this.resetPlacement = this.resetPlacement.bind(this);
  }

  resetPlacement(e){
    this.setState({
      placement: e.target.innerText,
      open: e.target.innerText !== this.state.placement ? true : !this.state.open,
      target: e.target,
    });
  }

  render() {
    return (
      <div>
        <Grid fluid>
          <Row style={{marginBottom: '15px'}}>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>top-start</Button>
            </Col>
            <Col xs={1} md={1} lg={1}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>top</Button>
            </Col>
            <Col xs={1} md={1} lg={1}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>top-end</Button>
            </Col>
            <Col xs={2} md={2} lg={2}></Col>
          </Row>

          <Row style={{marginBottom: '15px'}}>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>left-start</Button>
            </Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>right-start</Button>
            </Col>
          </Row>

          <Row style={{marginBottom: '15px'}}>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>left</Button>
            </Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>right</Button>
            </Col>
          </Row>

          <Row style={{marginBottom: '15px'}}>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>left-end</Button>
            </Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>right-end</Button>
            </Col>
          </Row>

          <Row>
            <Col xs={2} md={2} lg={2}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>bottom-start</Button>
            </Col>
            <Col xs={1} md={1} lg={1}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>bottom</Button>
            </Col>
            <Col xs={1} md={1} lg={1}></Col>
            <Col xs={2} md={2} lg={2}>
              <Button onClick={this.resetPlacement.bind(this)}>bottom-end</Button>
            </Col>
            <Col xs={2} md={2} lg={2}></Col>
          </Row>
        </Grid>
        <Popover open={this.state.open} target={this.state.target} placement={this.state.placement}>
          <div>
            这是一段文字，你可以在里面加入其他组件，如
            <Switch
              onChange={this.onChange} 
              defaultChecked={true} 
              label="switch开关" 
              textAlign="right"/>
          </div>
        </Popover>
      </div>
    );
  }
}
```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style|
| target | Element | undefined | popover参照点元素 |
| open | boolean | false | 是否显示popover |
| placement | 'auto-end'，'auto-start'，'auto'，'bottom-end'，'bottom-start'，'bottom'，'left-end'，'left-start'，'left'，'right-end'，'right-start'，'right'，'top-end'，'top-start'，'top'其中之一 | 'bottom' | popover位置 |
| showArrow | boolean | true | 是否显示箭头 |
| usePortal | boolean | true | 是否使用createPortal方式创建popover |