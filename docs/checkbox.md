# 复选框

```tsx
import { Checkbox } from '@casstime/bricks';

<Checkbox onToggle={this.handleToggle} defaultChecked={true} title="复选框" />
```

:::demo 示例

```tsx
class Example extends React.Component<{}, {}> {
  constructor(props: {}) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
  }
  handleToggle(checked: boolean) {
    console.log('checked', checked);
  }
  render() {
    return (
      <div>
        <Checkbox 
          onToggle={this.handleToggle}
          defaultChecked={true}
          title="复选框"
        />
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| name | string | undefined | 表单中的name属性 |
| className | string | '' | 自定义className |
| style | object | - | 自定义样式 |
| defaultChecked | boolean | false | 默认选中状态 |
| checked | boolean | undefined | 选中状态，如果传入checked，则组件受外部控制 |
| title | string | '' | 文字标题 |
| onToggle | Function(checked) | null | 变化时回调函数 |
| disabled | boolean | false | 是否禁用


## 受控

:::demo 示例

```tsx
interface IExampleState {
  checked: boolean;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: IExampleState) {
    super(props);
    this.handleToggle = this.handleToggle.bind(this);
    this.state = { checked: false };
  }
  handleToggle(checked: boolean) {
    console.log('checked', checked);
    // setState之前不会变更状态
    // this.setState({ checked });
  }
  render() {
    return (
      <div>
        <Checkbox 
          disabled
          onToggle={this.handleToggle}
          checked={this.state.checked}
          title="复选框"
        />
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::
