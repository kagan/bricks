# 下拉选择框(react)

```tsx
import { Select, IOption } from '@casstime/bricks';

<Select name="form-field-name" options={options} value={{value: '', label: '全部'}} onChange={this.onChange}
/>
```

## 一般下拉框 `(默认高度30px)`

:::demo
```tsx
interface IOption{
  value?: string;
  label?: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' , disabled: false },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' , disabled: false },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' , disabled: false },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' , disabled: false },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' , disabled: false },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' , disabled: true },
      ],
      value: { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)', disabled: false },
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
    this.setState({ value: option });
  }

  render() {
    return (
      <Select
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
      />
    );
  }
}
```
:::

## 给下拉框添加默认值
:::demo
```tsx
interface IOption{
  value?: string;
  label?: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' , disabled: false },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' , disabled: false },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' , disabled: false },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' , disabled: false },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' , disabled: false },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' , disabled: true },
      ]
    };

    this.onChange = this.onChange.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
  }

  render() {
    return (
      <Select
        name="form-field-name"
        options={this.state.options}
        defaultValue={{value: '', label: '请选择'}}
        onChange={this.onChange}
      />
    );
  }
}
```
:::

## 可筛选下拉框

:::demo
```tsx
interface IOption{
  value: string,
  label: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' },
      ],
      value: { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
    };

    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
    this.setState({ value: option });
  }

  onFocus(e: React.FocusEvent<HTMLInputElement>) {
    console.log('onFocus');
  }

  render() {
    return (
      <Select 
        type="search"
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
        onFocus={this.onFocus}
      />
    );
  }
}
```
:::

## 禁用下拉框

:::demo
```tsx
interface IOption{
  value: string,
  label: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' },
      ],
      value: { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
    };

    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
    this.setState({ value: option });
  }

  onFocus(e: React.FocusEvent<HTMLInputElement>) {
    console.log('onFocusonFocus');
  }

  render() {
    return (
      <Select 
        disabled 
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
        onFocus={this.onFocus}
      />
    );
  }
}
```
:::

## 小型下拉框 `(高度26px)`

:::demo
```tsx
interface IOption{
  value: string,
  label: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' },
      ],
      value: { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
    };

    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
    this.setState({ value: option });
  }

  onFocus(e: React.FocusEvent<HTMLInputElement>) {
    console.log('onFocus');
  }

  render() {
    return (
      <Select
        small
        type="search"
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
        onFocus={this.onFocus}
      />
    );
  }
}
```
:::

## 大型下拉框 `(高度36px)`

:::demo
```tsx
interface IOption{
  value: string,
  label: string;
  disabled?: boolean;
}

interface IDemoState{
  options: IOption[];
  value: IOption;
}

class Demo extends React.Component<{}, IDemoState>{
  constructor(props: {}) {
    super(props);

    this.state = {
      options: [
        { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
        { value: 'A:安捷物流(空)', label: 'A:安捷物流(空)' },
        { value: 'A:安联物流(汽)', label: 'A:安联物流(汽)' },
        { value: 'A:安能物流(汽)', label: 'A:安能物流(汽)' },
        { value: 'A:安麒物流(汽)', label: 'A:安麒物流(汽)' },
        { value: 'A:安麒鑫达(铁路)', label: 'A:安麒鑫达(铁路)' },
      ],
      value: { value: 'A:安捷物流(汽)', label: 'A:安捷物流(汽)' },
    };

    this.onChange = this.onChange.bind(this);
    this.onFocus = this.onFocus.bind(this);
  }

  onChange(option: IOption) {
    console.log('option', option);
    this.setState({ value: option });
  }

  onFocus(e: React.FocusEvent<HTMLInputElement>) {
    console.log('onFocus');
  }

  render() {
    return (
      <Select
        large
        type="search"
        name="form-field-name"
        options={this.state.options}
        value={this.state.value}
        onChange={this.onChange}
        onFocus={this.onFocus}
      />
    );
  }
}
```
:::

## Props 描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| name | string | '' | 表单的name字段 |
| value | Option | undefined | 表单当前选项 |
| defaultValue | Option | undefined | 默认表单选项 |
| options | Option[] | null | 必填，下拉菜单数据 |
| onChange | Function(Option) | null | 选中后的回调 |
| onFocus | Function(Event) | null | 获得焦点时回调 |
| large | boolean | false | 大型下拉框 |
| small | boolean | false | 小型下拉框 |
| disabled | boolean | false | 设置下拉框不可用,可以直接在组件加该属性,无需赋值 |

### Option 描述
| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| label | string | '' | 选项显示文本 |
| value | string | '' | 选项的值 |
| disabled | boolean | '' | 选项是否可选 |
