# 文本 Text

Text组件内部文字大小默认12px，#262626

```tsx
import { Text } from '@casstime/bricks';

<Text type="smallTitle" color="success" bold>文本</Text>
```
:::demo 文字综合示例

```tsx
class Demo extends React.Component<{}, {}>{
  render() {
    return (
      <div>
        <Row>
          <Col xs={3}><Text>默认文本</Text></Col>
          <Col xs={3}><Text type="smallTitle">小标题</Text></Col>
          <Col xs={3}><Text type="title">默认标题</Text></Col>
          <Col xs={3}><Text type="largeTitle">大标题</Text></Col>
        </Row>
        <Row>
          <Col xs={3}><Text color="highlight">高亮文本</Text></Col>
          <Col xs={3}><Text color="secondary">次要文本</Text></Col>
          <Col xs={3}><Text color="warn">警告文本</Text></Col>
          <Col xs={3}><Text color="success">成功文本</Text></Col>
        </Row>
        <Row>
          <Col xs={3}><Text color="info">info文本</Text></Col>
          <Col xs={3}><Text color="help">辅助文本</Text></Col>
          <Col xs={3}><Text color="blue">color="blue"</Text></Col>
          <Col xs={3}><Text color="#ff0000">color="#ff0000"</Text></Col>
        </Row>
        <Row>
          <Col xs={3}><Text bold>默认字体加粗</Text></Col>
          <Col xs={3}><Text type="smallTitle" bold>小标题加粗</Text></Col>
          <Col xs={3}><Text title="你好，欢迎使用bricks!">文本提示（鼠标放过来）</Text></Col>
          <Col xs={3}><Text ellipsis>省略号，这是一段非常非常长的文字，有人会喜欢这样的文字吗，不够长，那就再长一点...</Text></Col>
        </Row>
      </div>
    )
  }
}
```
:::

## Props

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
| type | string | '' | 可选，不填时渲染默认文本(12px), "smallTitle"(14px), "title"(16px)，"largeTitle"(20px) | 
| title | string | null | 可选，同原生title | 
| color | string | '' | 可选，"help": 辅助，"highlight": 高亮，"secondary": 次要，"warn": 警告，"success": 成功，“info”: 信息（链接），除此之外，还允许传入一般颜色如： #d3d3d3 | 
| bold | boolean | false | 可选，加粗 | 
| ellipsis | boolean | false | 可选，超过长度后显示省略号 | 
