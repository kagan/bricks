## 按钮 Button

> 1.0版将废弃纯css组件，请使用 [React按钮](#/button-react)

```html
<button class="button">按钮</button>
```
### 类型

按钮类型分为 primary(默认)、secondary、outline、dashed、text。

- 主按钮 `.button-primary`,允许缺省
- 次按钮 `.button-secondary`
- 边线按钮  `.button-outline`
- 虚线按钮 `.button-dashed`
- 文字按钮 `.button-text`

<br>
:::demo 按钮类型示例

```html
  <button class="button">默认按钮</button>
  <button class="button button-secondary">次级按钮</button>
  <button class="button button-outline">边线按钮</button>
  <button class="button button-dashed">虚线按钮</button>
  <button class="button button-text">文字按钮</button>
```
:::

### 形状
- 按钮的默认形状带2px圆角，如果添加`.button-rectangle`则圆角变成0，即矩形
- 如果添加`.button-round`显示半圆按钮


<br>
:::demo 按钮形状示例
```html
  <div>
    <button class="button button-rectangle">矩形按钮</button>
    <button class="button ">常规按钮</button>
    <button class="button button-round ">半圆按钮</button>
  </div>
```
:::

### 大小

按钮分为特大、大、中（默认）、小及特小几种类型。

- 特大按钮 `.button-xl`
- 大按钮 `.button-lg`
- 默认按钮 不需要额外添加class
- 小按钮 `.button-sm`
- 特小按钮 `.button-xs`

<br>
:::demo 按钮大小示例

```html
  <button class="button button-xl">特大按钮</button>
  <button class="button button-lg">大按钮</button>
  <button class="button">默认按钮</button>
  <button class="button button-sm">小按钮</button>
  <button class="button button-xs">特小按钮</button>
```
:::


### 胖按钮
一般按钮的高度和字体跟按钮的大小相关，但是如果设置了 button-fat,则按钮高度会膨胀至100%，可以用于某些需要和父组件保持高度一致的场景；
<br>
:::demo 按钮大小示例

```html
<div style="height:50px">
  <button class="button button-xl button-fat">特大的胖按钮</button>
  <button class="button button-xs button-fat">特小的胖按钮</button>
  <button class="button button-success button-fat">成功的胖按钮</button>
  <button class="button button-outline button-fat">外框胖按钮</button>
</div>
```
:::

### 颜色

按钮有不同的色彩方案，代表不同的情感偏好

- 成功 `.button-success`
- 警告 `.button-warn`

<br>
:::demo 按钮颜色示例

```html
  <button class="button">默认按钮</button>
  <button class="button button-secondary">次级按钮</button>
  <button class="button button-success">成功按钮</button>
  <button class="button button-secondary button-success">次级成功按钮</button>
  <button class="button button-warn">警告按钮</button>
  <button class="button button-secondary button-warn">次级警告按钮</button>
```
:::


### 宽度

按钮类型分为 长按钮、短按钮。

- 长按钮 `.button-long`, 给按钮增加 .button-long 后，按钮的宽度会设置成 100%，这样可以通过父元素控制按钮的宽度
- 短按钮 `.button-short` 短按钮没有设置最小宽度，一般比普通按钮要短

<br>
:::demo 按钮类型示例

```html
  <div>
    <button class="button button-primary button-long">长按钮</button>
  </div>
  <br>
  <button class="button button-primary button-short">短按钮</button>
  
```
:::

### 状态

按钮不可用时，需要添加 `.button-disabled`或者添加　｀disabled｀属性
<br>
:::demo 按钮类型示例
```html
  <div>
    <button class="button button-disabled">禁用</button>
    <button class="button" disabled>禁用按钮</button>
    <button class="button button-success" disabled>success禁用</button>
    <button class="button button-outline" disabled>outline禁用</button>
    <button class="button button-dashed" disabled>dashed禁用</button>
    <button class="button button-text" disabled>text禁用</button>
  </div>
```
:::


