# 按钮组 button-group

:::demo 按钮组型示例
```html
  <div class="button-group">
    <button class="button button-outline">按钮1</button>
    <button class="button button-outline button-active">按钮2</button>
    <button class="button button-outline">按钮3</button>
    <button class="button button-outline">按钮4</button>
  </div>
```
:::