# Carousel

```tsx
import { Carousel } from '@casstime/bricks';

<Carousel onChange={this.onChange}>
  <Carousel.Item>CarouselItem1</Carousel.Item>
  <Carousel.Item>CarouselItem2</Carousel.Item>
</Carousel>
```
### 一般使用
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: -1,
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    return (
      <Carousel onChange={this.onChange}
        style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
        >
        <Carousel.Item>
          CarouselItem1
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem2
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem3
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem4
        </Carousel.Item>
      </Carousel>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 受控的情况
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: 2,
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    const {index, activeIndex} = this.state;
    return (
      <div>
        {
          new Array(4).fill('').map((v, i) => {
            const index = i + 1;
            return (
              <Button 
                secondary={activeIndex === index} 
                outline={activeIndex !== index} 
                onClick={() => this.onChange(index)}>
                {index}
              </Button>
            )
          })
        }
        <Carousel onChange={this.onChange} activeIndex={activeIndex} autoPlay={false}
          style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
          >
          <Carousel.Item>
            CarouselItem1
          </Carousel.Item>
          <Carousel.Item>
            CarouselItem2
          </Carousel.Item>
          <Carousel.Item>
            CarouselItem3
          </Carousel.Item>
          <Carousel.Item>
            CarouselItem4
          </Carousel.Item>
        </Carousel>
      </div>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不允许自动播放
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: -1
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    return (
      <Carousel onChange={this.onChange} 
        style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
        autoPlay={false}
        >
        <Carousel.Item>
          CarouselItem1
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem2
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem3
        </Carousel.Item>
      </Carousel>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不允许鼠标滑动，只能自动播放
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: -1
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    return (
      <Carousel onChange={this.onChange} 
        style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
        swipeable={false}
        >
        <Carousel.Item>
          CarouselItem1
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem2
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem3
        </Carousel.Item>
      </Carousel>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不显示面板指示点
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: -1
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    return (
      <Carousel onChange={this.onChange} 
        style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
        showDots={false}
        >
        <Carousel.Item>
          CarouselItem1
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem2
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem3
        </Carousel.Item>
      </Carousel>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

### 不显示左右箭头
:::demo 示例
``` tsx
interface IExampleState {
  activeIndex: number;
}

class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      activeIndex: -1
    }
    this.onChange = this.onChange.bind(this);
  }

  onChange(index: number) {
    this.setState({
      activeIndex: index
    })
  }

  render() {
    return (
      <Carousel onChange={this.onChange} 
        style={{backgroundColor: 'rgba(0, 0, 0, 0.1)', height: '150px'}}
        showArrows={false}
        >
        <Carousel.Item>
          CarouselItem1
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem2
        </Carousel.Item>
        <Carousel.Item>
          CarouselItem3
        </Carousel.Item>
      </Carousel>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
::: 

## Carousel 组件 props 描述
| 属性 | 类型 | 默认值| 描述 |
| ---- | ----| ---- | ---- |
| className | string | '' | Carousel样式 |
| style |  React.CSSProperties  | {} | Carousel样式 |
| autoPlay | boolean | true | Carousel是否自动播放 |
| swipeable | boolean | true | Carousel是否可以鼠标滑动切换 |
| showDots | boolean | true | 是否显示面板指示点 |
| showArrows | boolean | true | 是否显示面板切换箭头 |
| onChange | function: (index:number) => void | 无 | 获取当前面板索引 | 
| activeIndex | boolean | undefined | 当前面板索引值 | 