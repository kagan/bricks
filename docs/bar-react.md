# Bar 横幅

```tsx
import { Bar } from '@casstime/bricks';

<Bar onGoBack={handleGoBack} showGoBackButton={true}>横幅组件</Bar>
```

## 一般使用

:::demo
```tsx
class Example extends React.Component {
  public render() {
    return (
      <Bar
      >
        横幅组件
      </Bar>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 展示返回按钮

:::demo
```tsx
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      showGoBack: true,
    };
  }

  public handleGoBack() {
    console.log('执行返回事件');
    alert('执行返回事件');
  }

  public render() {
    return (
      <Bar
        onGoBack={this.handleGoBack}
        showGoBackButton={this.state.showGoBack}
      >
        横幅组件
      </Bar>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## 组件右侧支持自定义封装

:::demo
```tsx
interface IExampleState {
  showGoBack: boolean;
}
class Example extends React.Component<{}, IExampleState> {
  constructor(props: {}) {
    super(props);
    this.state = {
      showGoBack: true,
    };
  }

  public handleTest() {
    console.log('执行返回事件');
    alert('test');
  }
  public handleGoBack() {
    console.log('执行返回事件');
    alert('执行返回事件');
  }

  public render() {
    return (
      <Bar  onGoBack={this.handleGoBack} showGoBackButton={this.state.showGoBack}>
        横幅组件
        <Bar.Extra>
          <Bar.ExtraButton onClick={this.handleTest} iconType="car" style={{background: '#f2f2f2'}}>test1</Bar.ExtraButton>
          <Bar.ExtraButton onClick={this.handleTest} iconType="refresh">刷新</Bar.ExtraButton>
        </Bar.Extra>
      </Bar>
    );
  }
}

ReactDOM.render(<Example />, document.getElementById(PLAYER_ID))
```
:::

## Bar组件 props描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| showGoBackButton | boolean | false | 是否显示返回按钮 |
| onGoBack | Function | undefined | 点击返回按钮的回调 |
| goBackButtonText | string | '' | 返回按钮文案支持自定义 |
| className | string | '' | 自定义className |
| style   | object | undefined | 自定义样式 |
| children | React.ReactNode | '' | 横条内容（标题） |

## ExtraButton子组件 Props描述

| 属性 | 类型 | 默认值 | 描述 |
|-----|------|-------|-----|
| iconType | string | '' | 图标自定义 |
| children | React.ReactNode | '' | 自定义文案内容 |
| onClick | Function | undefined | 点击事件 |
| className | string | undefined | 自定义className |
| style | React.CSSProperties | undefined | 自定义style |
