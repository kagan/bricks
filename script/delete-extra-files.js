const fs = require('fs');
const _ = require('lodash');
const THEMES_CONFIG = require('../themes.config.ts');

/**
 * 删除文件
 */
const deleteFile = (path) => {
  fs.exists(path, (exists) => {
    if (exists) {
      fs.unlink(path, function (error) {
        if (error) {
          console.error(error);
          return false;
        }
      })
    }
  })
}

/**
 * 删除生成的多余文件
 */
_.forEach(_.entries(THEMES_CONFIG), (configData) => {
  const [theme] = configData;
  if (theme !== 'default') {
    deleteFile(`src/styles/themes/_${theme}_variable.scss`);
  }
  deleteFile(`src/styles/bricks_${theme}.scss`);
  deleteFile(`dist/${theme}.production.js`);
  deleteFile(`dist/${theme}.production.js.map`);
  deleteFile(`dist/${theme}.development.js`);
});
