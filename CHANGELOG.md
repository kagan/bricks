# 更新日志


`bricks` 严格遵循 [Semantic Versioning 2.0.0](http://semver.org/lang/zh-CN/) 语义化版本规范。

## 发布周期

- 修订版本号：每周末会进行日常 bugfix 更新。（如果有紧急的 bugfix，则任何时候都可发布）
- 次版本号：每月发布一个带有新特性的向下兼容的版本。
- 主版本号：含有破坏性更新和新特性，不在发布周期内。

[//]: # (🌟：组件新增功能、优化项)
[//]: # (📖：文档修改、优化)
[//]: # (🐞：bug修复)

## 0.25.2

`2019-09-04`
- 🐞 修复Transfer组件层级过多时选中状态有误的bug


## 0.25.1

`2019-08-27`
- 🐞 修复Select选项disabled时可以被选中的bug

## 0.25.0

`2019-08-10`
- 🌟 优化Select组件下拉选项内容过长时显示不全的问题，给选项加title；
- 🌟 优化Input组件样式，使之符合最新UI规范；
- 🌟 优化GangedSelect组件，给选项增加disabled属性；
- 🐞 修复ChainSelect组件筛选功能失效的bug；
- 🐞 修复Pagination组件页码过长的bug

## 0.24.0

`2019-07-27`

- 🌟 基础组件新增onMouseover，onMouseout等事件，使组件调用更灵活；
- 🌟 优化Select组件选项过长时换行的问题，选项过长时显示省略号；
- 🌟 暴露常用组件的interface，如IOption、Itree等，便于开发使用；
- 📖 优化组件库文档，提供最新的组件文档地址，方便使用者收藏；
- 📖 优化组件库文档，对组件进行归类，方便更快找到所需组件；
- 📖 优化组件库文档，示例中补充引入组件及接口，方便开发使用;
- 🐞 修复transfer组件初始化状态下父节点勾选状态不正确的bug；
- 🐞 修复SpinBox组件禁用时颜色不符合规范的bug；

## 0.23.1

`2019-07-16`

- 🐞Tranfer组件右侧关闭部分选项,半选状态未更新bug
- 🐞ChainSelect组件受控时,数据展示bug

## 0.23.0

`2019-07-13`

- 💄新增ChainSelect组件
- 💄transfer增加半选效果
- ⚡️用svg重构progress组件
- ⚡️Toast增加Fade弹出效果
- ⚡️Select组件支持20000条数据
- ⚡️Progress组件,Toast组件,Tabs组件，ChainSelect组件新增单元测试
- 🐞Bar组件，SpinBox组件，Alert组件等组件样式修改
- 🐞解决在小屏上左侧菜单不能滚动到最底部的bug
- 🐞解决IE9浏览器下toast组件样式错乱的bug
- 🌟暴露出VirtualizedList，方便复用
- 🌟废弃Dialog组件，提供Modal替换的方案

## 0.22.0

`2019-06-28`

- 💄 能够对组件进行主题定制
- 💄 新增Alert组件
- 🐞 Bar组件右侧按钮的宽度支持自适应
- 🐞 Tab组件支持hover效果
- ⚡️ Carousel组件增加受控特性
- ⚡️ Transfer组件新增disable属性
- ⚡️ Select组件支持defaultValue值
- ⚡️ Menu组件样式优化
- 🌟 文档适配手机，便于浏览

## 0.21.0

`2019-06-15`

- 💄 新增bricks组件库，更新日志和变更详情说明。
- ⚡️ 重构Tabs组件，使调用更方便，更容易自定义。[#IRCWV](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=IRCWV)
- ⚡️ 重构Spinner组件，使用svg。[#IUS7K](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=IUS7K)
- 🐞 修复Modal组件head的文本，设置行高后，样式混乱。[#IXAT0](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=IXAT0)
- ⚡️ 重构Bar组件，新增一个可控的“刷新”按钮，高度等样式可自定义。[#IXAT7](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=IXAT7)，[#ITRZV](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=ITRZV)
- 🌟 完善Carousel，缺少左右箭头。[#IW6U6](https://gitee.com/cass-rdc/dashboard/issues?project_id=5799407&id=IW6U6)

## 0.20.0

`2018-04-13`

- 💄 新增Carousel轮播图组件
- 💄 新增Popover弹出层组件
- ⚡️ 规范组件库的滚动条样式，全局封装滚动条
- ⚡️ 重构Checkbox 组件，新增禁用样式
- ⚡️ 重构transfer组件，使组件能够支持复杂的业务场景
- 🐞 修复SpinBox组件默认值不能设为0的bug
- 🐞 修复 DatePicker 组件 disabled 状态下 hover 会变色的bug
- 🐞 修复Text 组件 type 和 color 属性相互干扰的bug

## 0.19.1

`2018-01-21`

- 💄 新增部分组件的单元测试
- 🌟 支持 Es Module 导出
- ⚡️ Date-picker 组件支持显示时分，支持自定义分钟维度
- ⚡️ 重构了 Fade 组件
- ⚡️ Backdrop 组件和 Modal 组件引入了Fade组件
- 🐞 修复了 Spinner 组件图片显示异常的bug

## 0.18.0

`2018-12-19`

- ⚡️ 重构Menu组件
- 🌟 测试框架替换为Jest + Enzyme
- 💄 新增部分组件的单元测试
- 🌟 文档中的组件示例替换为typescript
- 🌟 文档中配置tsx语法高亮

## 0.17.2

`2018-12-05`

- 💄 新增Switch组件
- 🌟 Input组件支持数值类型
- 🌟 完善icon图标库
- 🌟 规范表单组件UI
- 🐞 修复了date-picker组件“今天”选择异常的bug

## 0.16.1

`2018-11-29`

- 💄 新增Spinner组件
- 🌟 Toast组件新增loading功能
- 🌟 Confirm组件支持自定义按钮文字
- 🌟 Input组件支持type、maxLength等属性
- 🐞 修复了Select组件点击不能收缩事件bug
- 🐞 修复了Tabs样式bug
- 🌟 bricks-sample 兼容至IE9 

## 0.15.0

`2018-11-21`

- 💄 新增DateRangePicker组件，支持按年、月、季度等选择时间段
- 💄 新增Dropdown组件，支持在任意组件下打开下拉框
- 🌟 Panel的header属性设置为可选值，扩大Panel的使用场景
- 🐞 修复Select组件滚动条位置样式异常的bug
- 🐞 修复若干文档示例错误
- 🌟 所有组件都支持自定义className及style
- 💄 增加git hooks，push 代码前自动执行单元测试
- 🌟 支持Jenkins一键发布

## 0.14.0

`2018-10-17`

- 💄 新增Fade组件
- 💄 新增Accordion组件
- 💄 新增ToolTip组件
- ⚡️ 重新封装Collapse组件
- 🌟 DatePicker除日期选择外还支持时间选择
- 🌟 SpinBox支持自定义className和style
- 🌟 withValidator当规则为function时，增加第二个参数为values引用
- 🐞 修复withValidator在TypeScript使用时类型错误的bug
- 🐞 修复DatePicker在TypeScript使用时类型错误的bug
- 🌟 从组件库中暴露 moment 时间库
- 🌟 从组件库中暴露所有组件的props interface
- 🌟 文档菜单使用Accordion组件重构
- 🌟 文档框架支持TypeScript

## 0.13.2

`2018-09-21`

- 💄 新增Modal通用组件
- 🐞 修复Table组件padding过大的bug
- 🐞 修复Table组件ts声明缺失的bug
- 🐞 修复DatePicker样式不符合规范的bug


## 0.12.0

`2018-09-14`

- 💄 新增Tooltip组件
- ⚡️ Bar组件增加React封装
- 🌟 Toast组件支持函数调用方式显示
- 🌟 Tag组件增加title属性
- 🌟 Table允许换行
- ⚡️ js库大小由700K优化到500K
- ⚡️ 优化文档显示效果
- ⚡️ 代码重构
- 🐞 修复若干bug

## 0.11.0

`2018-09-07`

- 💄 新增表单相关组件(FormGroup,FormLabel,FormControl,FormHelp)
- 💄 新增Toast组件
- 🌟 优化文档显示效果，支持代码高亮
- ⚡️ 重构Text组件，修复title属性语义模糊的bug
- 🌟 优化构建配置，支持自动添加css前缀
- 🌟 优化脚手架配置，加快编译速度
- 🌟 移除废弃组件代码(旧版Tranfer及旧版Tree)
- 🐞 修复Dialog组件关闭图标错误的bug
- 🐞 修复树组件只有单层数据时过多留白的bug
- 🐞 修复Tabs组件下划线错位的bug

## 0.10.0

`2018-09-01`

- 💄 新增Confirm组件 
- 🌟 Button封装成React组件 
- 🌟 Select组件增加多选功能 
- 💄 增加表单校验功能 
- ⚡️ 使用BEM重构css 
- ⚡️ 重构Tree组件
- 🐞 修复大量的BUG

## 0.9.0

`2018-08-24`

- 🌟 表单组件允许通过属性控制高度
- 🌟 树组件性能优化
- 🌟 完善Select组件
- ⚡️ 重构Transfer组件
- 🐞 BUG修复

## 0.8.0

`2018-08-18`

- 💄 新增步骤条
- 💄 新增Steps组件
- 💄 新增Menu组件
- 🌟 Select组件增加筛选功能
- ⚡️ 规范组件样式，提升用户体验
- 💄 引入单元测试框架
- 🐞 修复遗留Bug

## 0.7.1

`2018-08-10`

- 💄 新增面包屑组件
- 💄 新增Tag组件
- 💄 新增进度条LineProgress组件
- 🌟 将bricks发布到npm
- ⚡️ 完善示例工程配置
- 💄 增加开发者文档
- 🐞 修复穿梭框中搜索框不能过滤的bug [issue#ILU3B]((https://gitee.com/cassfrontend/bricks/issues/IKUSQ?from=project-issue))
- 🐞 修复SpinBox组件样式错误的bug [issue#ILU44]((https://gitee.com/cassfrontend/bricks/issues/ILU44?from=project-issue))
- 🐞 修复ts定义文件指向不正确的bug [issue#ILVVA]((https://gitee.com/cassfrontend/bricks/issues/ILVVA?from=project-issue))
- 🐞 修复折叠组件激活时含有outline的bug [issue#ILW4B]((https://gitee.com/cassfrontend/bricks/issues/ILW4B?from=project-issue))

## 0.6.0

`2018-08-04`

- 💄 新增SpinBox组件(React)
- 💄 新增Radio组件(React)
- 💄 新增button-group组件(css)
- 💄 新增TimeLine组件（React）
- 💄 新增一些utils class (css)
- ⚡️ Input组件(React) 支持自定义尾部图标
- 🌟 DatePicker组件(React) 增加禁止选择指定范围日期功能
- ⚡️ 完善了多个组件的文档
- ⚡️ 初步支持非React工程调用

## 0.5.0

`2018-07-27`

- 🐞 修复下拉框组件指示器图标不正确的bug issue#ILG24
- 🐞 修复Tabs组件页签颜色不对的bug issue#ILG25
- 🐞 修复穿梭框颜色和图标不正确的bug issue##ILG23
- 💄 增加checkbox组件

## 0.4.0

`2018-07-21`

- 💄 穿梭框组件
- 💄 页签组件
- 💄 日历组件
- 💄 输入框样式
- 💄 级联选择下拉框组件
- 💄 对话框组件
- 💄 表格组件
- 💄 折叠组件
- 💄 横条样式

## 0.3.0

`2018-07-13`

- 💄 新增下拉选择组件
- 💄 新增分页组件
- 💄 新增面板组件
- 💄 新增树结构组件
- 💄 支持npm安装

## 0.2.0

`2018-07-07`

- 💄 新增文本类样式及文档
- 💄 新增图标库及文档

## 0.0.1

`2018-06-29`

- 💄 新增按钮类样式及文档
- 🐞 修复button在内容为数字和汉字时显示不一致的问题。 [#IKUSQ](https://gitee.com/cassfrontend/bricks/issues/IKUSQ?from=project-issue)