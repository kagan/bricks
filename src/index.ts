import moment from 'moment';
import { AutoSizer, List, ListRowProps } from 'react-virtualized';
import warning from 'warning';

(() => {
  warning(false, '0.23.0版本起Dialog组件已废除，请使用Modal替换！');
})();

import './style';

import * as utils from './utils/utility';

export * from './dom';

export { utils };

export {AutoSizer, List};
export type ListRowProps = ListRowProps;

export { moment };

export * from './hoc';
export * from './components/form';
export * from './components/grid';
export * from './components/row';
export * from './components/col';
export * from './components/icon';
export * from './components/text';
export * from './components/button';
export * from './components/checkbox';
export * from './components/select';
export * from './components/multiselect';
export * from './components/gangedselect';

export * from './components/tree/VirtualizedTree';
export * from './components/tree';
export * from './components/transfer';

export * from './components/pagination';
export * from './components/panel';

export * from './components/tabs';

export * from './components/datepicker';
export * from './components/table';

export * from './components/collapse';
export * from './components/daterangepicker';
export * from './components/accordion';
export * from './components/fade';

export * from './components/radio';
export * from './components/spinbox';
export * from './components/input';
export * from './components/timeline';
export * from './components/steps';
export * from './components/progress';
export * from './components/tag';
export * from './components/breadcrumb';
export * from './components/menu';
export * from './components/bar';

export * from './components/backdrop';
export * from './components/dropdown';
export * from './components/confirm';
export * from './components/modal';
export * from './components/toast';
export * from './components/tooltip';
export * from './components/spinner';
export * from './components/switch';
export * from './components/carousel';
export * from './components/popover';
export * from './components/cascader';
export * from './components/alert';
export * from './components/chainselect';
