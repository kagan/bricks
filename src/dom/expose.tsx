import { EventEmitter } from 'events';
import React from 'react';

export default function expose(Com: React.ComponentClass, props: any = null) {
  return class extends React.Component<any, any> {
    constructor(newProps: any) {
      super(newProps);
      this.state = { props };
      this.update = this.update.bind(this);
    }

    public update(newProps: any) {
      this.setState({ props: { ...this.state.props, ...newProps } });
    }

    public render() {
      return <Com {...this.state.props} />;
    }
  };
}
