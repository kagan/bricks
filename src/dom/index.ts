import React from 'react';
import ReactDom from 'react-dom';
import expose from './expose';

export const mount = (Com: any, props: React.Props<any>, root: Element) => {
  const wrapper = expose(Com, props);
  let ref: any = null;
  const ele = React.createElement(wrapper, { ...props, ref: (r) => { ref = r; } });
  ReactDom.render(ele, root);
  return {
    update(newProps: any) {
      if (ref) {
        ref.update(newProps);
      } else {
        // tslint:disable-next-line
        console.warn('组件尚未挂在，不能调用update');
      }
    },
  };
};
