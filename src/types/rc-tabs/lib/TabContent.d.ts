import React from 'react';

interface ITabContent {
  style?: React.CSSProperties; // tab content style
  animated?: boolean;	// default true	whether tabpane change with animation
  animatedWithMargin?: boolean; // default	false	whether animate tabpane with css margin
}

export default class TabContent extends React.Component<ITabContent> {
}
