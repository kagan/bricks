import React from 'react';

interface IFunction {
  readonly name: string;
}

export type TabsType = 'line' | 'card' | 'editable-card';
export type TabsPosition = 'top' | 'right' | 'bottom' | 'left';

export interface IRcTabPaneProps {
  key: string;
  tab: string;
  style?: React.CSSProperties;
  placeholder?: React.ReactNode;
  forceRender?: boolean; // default false;
  activeKey?: string;
  defaultActiveKey?: string;
  hideAdd?: boolean;
  onChange?: (activeKey: string) => void;
  onTabClick?: IFunction;
  onPrevClick?: React.MouseEventHandler<any>;
  onNextClick?: React.MouseEventHandler<any>;
  tabBarExtraContent?: React.ReactNode | null;
  tabBarStyle?: React.CSSProperties;
  type?: TabsType;
  tabPosition?: TabsPosition;
  onEdit?: (targetKey: string | React.MouseEvent<HTMLElement>, action: any) => void;
  size?: 'large' | 'default' | 'small';
  prefixCls?: string;
  className?: string;
  animated?: boolean | { inkBar: boolean; tabPane: boolean; };
  tabBarGutter?: number;
}

export interface IRcTabsProps {
  activeKey?: string; //current active tabPanel's key
  tabBarPosition?: string; //tab nav 's position. one of ['left','right','top','bottom']
  defaultActiveKey?: string; //first active tabPanel's key	initial active tabPanel's key if activeKey is absent
  renderTabBar: React.ReactNode; //How to render tab bar
  renderTabContent: React.ReactNode; //How to render tab content
  onChange?: (key: string) => void; //called when tabPanel is changed
  destroyInactiveTabPane?: boolean; // default	false	whether destroy inactive tabpane when change tab
  prefixCls?: string;	// default rc-tabs	prefix class name, use to custom style
}

export class TabPane extends React.Component<IRcTabPaneProps> {
}

export default class Tabs extends React.Component<IRcTabsProps> {
}
