import React from 'react';

export interface IReactPaginationLocale {
  items_per_page: string;
  jump_to: string;
  jump_to_confirm: string;
  page: string;
  prev_page: string;
  next_page: string;
  prev_5: string;
  next_5: string;
  prev_3: string;
  next_3: string;
}

export interface IReactPaginationProps {
  defaultCurrent?: number; // 默认当前页，　１
  current?: number; // 当前页
  total: number;
  defaultPageSize?: number;
  pageSize?: number;
  onChange?: (current: number, pageSize: number) => void;
  showSizeChanger?: boolean;
  pageSizeOptions?: Array<string[]>,
  onShowSizeChange?: (current: number, size: number) => void;
  hideOnSinglePage?: boolean;
  showPrevNextJumpers?: boolean;
  showQuickJumper?: boolean | { goButton: React.ReactNode };
  showTotal?: (total: number, from?: number, to?: number) => void;
  className?: string;
  simple?: any;
  locale?: IReactPaginationLocale | null;
  style?: React.CSSProperties;
  showLessItems?: boolean;
  showTitle?: boolean;
  itemRender?: (current: number, type: 'prev' | 'next', element: React.ReactNode) => React.ReactNode
}

export default class ReactPaginationClass extends React.Component<IReactPaginationProps> {
}
