import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface ITagProps extends IDOMProps {
  onClick?: () => void;
  onClose?: () => void;
  children: React.ReactNode;
  className?: string;
  style?: React.CSSProperties;
  title?: string;
}

export const Tag: React.SFC<ITagProps> = (props: ITagProps) => {
  const { className, style, children, title, onClick, onClose, ...domProps } = props;
  const handleClose = (event: React.MouseEvent) => {
    if (onClose) {
      event.stopPropagation();
      onClose();
    }
  };
  return (
    <span
      {...domProps as IDOMProps}
      title={title}
      className={classNames('br-tag', className)}
      style={style}
    >
      <span onClick={onClick}>{children}</span>
      <i onClick={handleClose} className="icon icon-close-thin"></i>
    </span>
  );
};

Tag.propTypes = {
  onClick: PropTypes.func,
  onClose: PropTypes.func,
  children: PropTypes.node.isRequired,
  className: PropTypes.string,
  title: PropTypes.string,
};
