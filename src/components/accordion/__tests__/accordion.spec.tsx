import { shallow } from 'enzyme';
import React from 'react';
import { Accordion } from '../Accordion';
import { AccordionPanel } from '../AccordionPanel';

describe('accordion 单元测试', () => {
  test('AccordionPanel 测试', () => {
    const accordion = shallow(<AccordionPanel index={1} disabled={false}></AccordionPanel>);
    expect(accordion.prop('className')).toEqual('br-accordion__panel');
  });

  test('Accordion 测试', () => {
    const accordion = shallow(<Accordion>
    </Accordion>);
    expect(accordion.prop('className')).toEqual('br-accordion');
  });

  test('模拟点击 active', () => {
    const onAccordionPanelHeaderClick = jest.fn();
    const accordion = shallow(
      <AccordionPanel index={3} disabled={false} active={true}
        onToggle={onAccordionPanelHeaderClick}></AccordionPanel>);
    expect(accordion.find('div.br-accordion__panel__header').prop('className'))
      .toEqual('br-accordion__panel__header active');
    accordion.find('div.br-accordion__panel__header').simulate('click');
    expect(onAccordionPanelHeaderClick.mock.calls.length).toBe(1);
    expect(onAccordionPanelHeaderClick.mock.calls[0][0]).toBe(3);
    expect(accordion).toMatchSnapshot();
  });

  test('disabled 测试', () => {
    const onAccordionPanelHeaderClick = jest.fn();
    const accordion = shallow(<AccordionPanel index={1} disabled={true} active={true}
      onToggle={onAccordionPanelHeaderClick}></AccordionPanel>);
    accordion.find('div.br-accordion__panel__header').simulate('click');
    expect(onAccordionPanelHeaderClick.mock.calls.length).toBe(0);
  });
});
