import PropTypes from 'prop-types';
import React from 'react';
import classNames from 'classnames';

import { Collapse } from '../collapse';
import { Icon } from '../icon';

import { IDOMProps } from '../../utils/props';

export interface IAccordionPanelProps extends IDOMProps {
  className?: string;
  style?: React.CSSProperties;
  index: number;
  title?: React.ReactNode;
  active?: boolean;
  disabled: boolean;
  onToggle?: (index: number) => void;
}

export interface IAccordionPanelState {
  active?: boolean;
}

export class AccordionPanel extends React.Component<IAccordionPanelProps, IAccordionPanelState> {
  public static propTypes = {
    className: PropTypes.string,
    style: PropTypes.object,
    children: PropTypes.node,
    title: PropTypes.node,
    active: PropTypes.bool,
    disabled: PropTypes.bool,
    onToggle: PropTypes.func,
  };

  public static defaultProps = {
    disabled: false,
    active: false,
    title: '',
  };

  constructor(props: IAccordionPanelProps) {
    super(props);

    this.onToggle = this.onToggle.bind(this);
  }

  public onToggle() {
    const { onToggle, index, disabled } = this.props;
    if (!disabled && onToggle) {
      onToggle(index);
    }
  }

  public render() {
    const { className, style, disabled, active, title, children, ...domProps } = this.props;
    return (
      <div
        {...domProps as IDOMProps}
        className={classNames('br-accordion__panel', className)}
        style={style}
      >
        <div className={classNames('br-accordion__panel__header', {
          disabled,
          active,
        })}
          onClick={this.onToggle}>
          {title}
          <Icon className={classNames('br-accordion__header__icon pull-right ', {
            active,
          })}
            size={14} type="down" onClick={this.onToggle} />
        </div>
        <Collapse active={active} className="br-accordion__panel__content">
          {children}
        </Collapse>
      </div>
    );
  }
}
