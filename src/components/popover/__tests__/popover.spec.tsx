import { shallow } from 'enzyme';
import React from 'react';
import { Popover } from '../Popover';

describe('popover组件单元测试', () => {
  test('popover属性测试', () => {
    const popover = shallow(
      <Popover open={false} className="custom-popover">
        <div className="popover-custom-content">123456</div>
      </Popover>,
    );
    expect(popover.prop('open')).toBeFalsy();
    expect(popover.state('open')).toBeFalsy();
  });
});
