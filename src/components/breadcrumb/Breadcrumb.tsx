import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

import { Icon } from '../icon';

export interface IBreadcrumbProps extends IDOMProps {
  description?: string;
  children?: React.ReactNode;
  icon?: string;
  className?: string;
  style?: React.CSSProperties;
}

export interface IBreadcrumbItemProps {
  children?: React.ReactNode;
  active?: boolean;
}

const Item = (props: IBreadcrumbItemProps) => {
  return (
    <li className={classNames('br-breadcrumb__item', props.active ? 'br-breadcrumb__item--active' : '')}>
      {props.children}
    </li>
  );
};

export class Breadcrumb extends React.PureComponent<IBreadcrumbProps, {}> {
  public static propTypes = {
    description: PropTypes.string,
    children: PropTypes.node,
    icon: PropTypes.string,
    className: PropTypes.string,
    style: PropTypes.object,
  };
  public static Item = Item;
  public render() {
    const { className, style, icon, description, children, ...domProps } = this.props;
    return (
      <ol
        {...domProps as IDOMProps}
        className={classNames('br-breadcrumb', className)}
        style={style}
      >
        {icon && <Icon className="br-breadcrumb__icon" type={icon} />}
        <span className="text">{description || ''}</span>
        {children}
      </ol>
    );
  }
}
