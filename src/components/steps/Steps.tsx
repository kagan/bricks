import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IStepsProps extends IDOMProps {
  current?: number; // 0	index of current step
  className?: string;
  style?: React.CSSProperties;
  children?: JSX.Element | JSX.Element[] | string;
}

export interface IStepProps {
  iconClassName?: string;
  className?: string;
  title?: string;
  style?: React.CSSProperties;
  char?: number | string;
  width?: string;
}

export interface IStepsState {
  current?: number;
}

const Item: React.SFC<IStepProps> = (props: IStepProps) => {
  const tag = props.iconClassName ? <i className={classNames('icon', props.iconClassName)} /> : props.char;
  return (
    <li style={{ width: props.width }} className={classNames('step', props.className)}>
      <div className="step-line"></div>
      <div className="step-node"><pre>{tag}</pre></div>
    </li>
  );
};
Item.propTypes = {
  iconClassName: PropTypes.string,
  className: PropTypes.string,
  title: PropTypes.string,
  style: PropTypes.object,
  char: PropTypes.number || PropTypes.string,
  width: PropTypes.string,
};

const TextItem: React.SFC<IStepProps> = (props: IStepProps) => {
  return (
    <li style={{ width: props.width }} className={classNames('step', props.className)}>
      <div className="step-title text-ellipsis">{props.title}</div>
    </li>
  );
};

export class Steps extends React.PureComponent<IStepsProps, IStepsState> {
  public static propTypes = {
    iconClassName: PropTypes.string,
    className: PropTypes.string,
    title: PropTypes.string,
    style: PropTypes.object,
    width: PropTypes.string,
  };
  public static Item = Item;
  public static TextItem = TextItem;

  public render() {
    const { children, className, style, current: currentStep, ...domProps } = this.props;
    const length = React.Children.count(children);
    if (!length) {
      return null;
    }
    let current = currentStep;
    const steps = React.Children.map(children, (child, index) => {
      if (!child || !(child as any).props) {
        return null;
      }
      const props = (child as any).props;
      let status = 'wait';
      if (current === index) {
        status = 'process';
      } else if (current as number > index) {
        status = 'finish';
      }

      return (
        <Item
          key={index}
          char={props.char || index + 1}
          width={`${100 / length}%`}
          className={classNames(status)}
          iconClassName={props.iconClassName}
        />
      );
    });

    const textSteps = React.Children.map(children, (child, index) => {
      if (!child || !(child as any).props) {
        return null;
      }
      const props = (child as any).props;
      let status = 'wait';
      if (current === index) {
        status = 'process';
      } else if (current as number > index) {
        status = 'finish';
      }

      return (
        <TextItem
          key={index}
          width={`${100 / length}%`}
          title={props.title}
          className={classNames(status)}
        />
      );
    });

    return (
      <div
        {...domProps as IDOMProps}
        className={`steps-container ${className || ''}`}
        style={style}
      >
        <ul className="steps">{steps}</ul>
        <ul className="steps">{textSteps}</ul>
      </div>
    );
  }
}
