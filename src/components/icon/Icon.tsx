import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IconProps extends IDOMProps {
  type: string;
  size?: number;
  color?: string;
  className?: string;
  onClick?: React.MouseEventHandler<Element>;
  // title?: string;
  // onClick?: React.MouseEventHandler<any>;
  // spin?: boolean;
  style?: React.CSSProperties;
}

export const Icon: React.SFC<IconProps> = (props: IconProps) => {
  const { type, size, color, ...domProps } = props;
  const classString = classNames('icon', `icon-${type}`, props.className);
  const style = {
    fontSize: `${size}px`,
    color: `${color}`,
    ...props.style,
  };
  return (
    <i
      {...domProps as IDOMProps}
      onClick={props.onClick}
      className={classString}
      style={style}
    />
  );
};

Icon.propTypes = {
  type: PropTypes.string.isRequired,
  size: PropTypes.number,
  color: PropTypes.string,
  className: PropTypes.string,
  style: PropTypes.object,
};
