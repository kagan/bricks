import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

type SpecialColor = 'help' | 'highlight' | 'secondary' | 'info' | 'warn' | 'success';

export interface ITextProps extends IDOMProps {
  type?: 'help' | 'link' | 'title' | 'smallTitle' | 'largeTitle'; // help 和 link 兼容旧版
  className?: string;
  style?: React.CSSProperties;
  title?: string; // 标题
  small?: boolean;
  large?: boolean;
  xLarge?: boolean;
  bold?: boolean;
  color?: SpecialColor | string;
  ellipsis?: boolean;
  children?: React.ReactNode;
}

export const Text: React.SFC<ITextProps & React.HTMLAttributes<HTMLSpanElement>> =
  (props: ITextProps & React.HTMLAttributes<HTMLSpanElement>) => {
    const { title, type, color, bold, ellipsis, className, style, ...domProps } = props;

    const classes = {
      'br-text': true,
      'br-text--title': type === 'title',
      'br-text--title-sm': type === 'smallTitle',
      'br-text--title-lg': type === 'largeTitle',
      'br-text--ellipsis': ellipsis,
      'br-text--help': type === 'help' || color === 'help',
      'br-text--highlight': color === 'highlight',
      'br-text--secondary': color === 'secondary',
      'br-text--info': type === 'link' || color === 'info',
      'br-text--warn': color === 'warn',
      'br-text--success': color === 'success',
      'br-text--bold': bold,
    };

    const finalStyle: React.CSSProperties = { ...style };

    const isSpecialColor = ['help', 'highlight', 'secondary', 'info', 'link', 'warn', 'success'].includes(color || '');

    // 自定义其他颜色
    if (!isSpecialColor && color) {
      finalStyle.color = color;
    }

    return (
      <span
        {...domProps as IDOMProps}
        style={finalStyle}
        title={title} className={classNames(classes, className)}
      >
        {props.children}
      </span>
    );
  };

Text.propTypes = {
  type: PropTypes.oneOf(['help', 'link', 'title', 'smallTitle', 'largeTitle']), // help 和 link 兼容旧版
  className: PropTypes.string,
  style: PropTypes.object,
  title: PropTypes.string, // 标题
  small: PropTypes.bool,
  large: PropTypes.bool,
  xLarge: PropTypes.bool,
  bold: PropTypes.bool,
  ellipsis: PropTypes.bool,
  children: PropTypes.node,
};
