import PropTypes from 'prop-types';
import React from 'react';
import { Fade } from '../fade';
import warning from 'warning';

import { IDOMProps } from '../../utils/props';

export interface IBackdropProps extends IDOMProps {
  className?: string;
  visible: boolean;
  onBackdropClick?: () => void;
}

export class Backdrop extends React.Component<IBackdropProps, {}> {

  public static defaultProps = {
    className: '',
  };

  public static propTypes = {
    visible: PropTypes.bool,
    onBackdropClick: PropTypes.func,
  };

  constructor(props: IBackdropProps) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  public handleClick() {
    if (this.props.onBackdropClick) {
      this.props.onBackdropClick();
    }
  }

  public render() {
    warning(!this.props.children, '0.18.0版本起Backdrop组件的children属性已经移除，请注意使用');
    const { className, visible, ...domProps } = this.props;
    return (
      <Fade
        {...domProps as IDOMProps}
        className={`br-backdrop ${className}`}
        active={visible}
        onClick={this.handleClick}
      />
    );
  }
}
