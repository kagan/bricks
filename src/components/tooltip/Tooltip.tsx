import RcTooltip, { RCTooltip } from 'rc-tooltip';
import React from 'react';

export const Tooltip: React.SFC<RCTooltip.Props> = (props: RCTooltip.Props) => {
  return <RcTooltip
    {...props}
  />;
};
