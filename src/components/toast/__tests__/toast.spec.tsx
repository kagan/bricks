import { shallow } from 'enzyme';
import React from 'react';
import { Toast } from '../Toast';
import { Fade } from '../../fade/Fade';
import { Icon } from '../../icon/Icon';
import { Spinner } from '../../spinner/Spinner';

describe('Toast 组件测试', () => {
  test('Toast 基础测试', () => {
    const toast = shallow(<Toast ></Toast>);
    expect(toast.find(Fade).prop('className')).toEqual('br-toast');
    expect(toast.find('div.br-toast__body').prop('className')).toEqual('br-toast__body');
    expect(toast.find('div.br-toast__content').prop('className')).toEqual('br-toast__content');
  });

  test('Toast success弹窗测试', () => {
    const icon = 'check-circle';
    const color = '#43b52c';
    const toast = shallow(<Toast icon={icon} color={color}></Toast>);
    expect(toast.find(Icon).prop('className')).toEqual('br-toast__icon');
    expect(toast.find(Icon).prop('type')).toEqual(icon);
    expect(toast.find(Icon).prop('color')).toEqual(color);
  });

  test('Toast loading弹窗测试', () => {
    const toast = shallow(<Toast loading></Toast>);
    expect(toast.find(Spinner).prop('className')).toEqual('br-toast__gifIcon');
  });

  test('Toast hide方法测试', () => {
    const mockFn = jest.spyOn(Toast.prototype, 'hide');
    const toast = Toast.success('保存成功！');
    toast.hide();
    expect(mockFn).toBeCalled();
    mockFn.mockRestore();
  });

  test('Toast success方法测试', () => {
    const mockFn = jest.spyOn(Toast, 'success');
    const toast = Toast.success('保存成功！');
    expect(mockFn).toBeCalled();
    mockFn.mockRestore();
  });

  test('Toast loading 方法测试', () => {
    const mockFn = jest.spyOn(Toast, 'loading');
    const toast = Toast.loading('内容正在加载中');
    expect(mockFn).toBeCalled();
    mockFn.mockRestore();
  });
});
