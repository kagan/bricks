import classNames from 'classnames';
import React from 'react';

import { IDOMProps } from '../../utils/props';

import { MenuItem } from './MenuItem';
import { SubMenu } from './SubMenu';

export interface IMenuProps extends IDOMProps {
  className?: string;
  style?: React.CSSProperties;
  vertical?: boolean;
}

export class Menu extends React.Component<IMenuProps, {}> {
  public static Item: typeof MenuItem;
  public static SubMenu: typeof SubMenu;
  public render() {
    const {
      className,
      style,
      vertical,
      ...domProps
    } = this.props;
    return (
      <ul
        {...domProps as IDOMProps}
        className={classNames('br-menu', className, {
          'br-menu--vertical': vertical,
        })}
        style={style}
      >
        {this.props.children}
      </ul>
    );
  }
}

Menu.Item = MenuItem;
Menu.SubMenu = SubMenu;
