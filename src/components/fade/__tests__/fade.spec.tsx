import * as React from 'react';

import { Fade } from '../Fade';

import { shallow, mount } from 'enzyme';

describe('fade组件测试', () => {
  test('fade out', () => {
    const fadeout = shallow(<Fade active={false}><div style={{width: '100px'}}>123</div></Fade>);
    expect(fadeout.prop('items')).toEqual(false);
    expect(fadeout.prop('config')).toEqual({
      duration: 200,
      precision: 0.1,
      tension: 170,
      friction: 26,
    });
    expect(fadeout.prop('from')).toEqual({
      opacity: 0,
    });
    expect(fadeout.prop('enter')).toEqual({
      opacity: 1,
    });
    expect(fadeout.prop('leave')).toEqual({
      opacity: 0,
    });
    expect(fadeout).toMatchSnapshot();
    expect(fadeout.render()).toMatchSnapshot();

    fadeout.setProps({active: true});
    expect(fadeout.prop('items')).toEqual(true);
  });

  test('fade in', () => {
    const fadein = mount(<Fade active={true} style={{width: '100px'}}>123</Fade>);
    expect(fadein.find('div').prop('style')).toEqual({
      width: '100px',
      willChange: 'opacity, transform',
      opacity: 0,
    });
  });
});
