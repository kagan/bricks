import * as React from 'react';
import { shallow, mount } from 'enzyme';

import { Carousel } from '../Carousel';
import { CarouselItem } from '../CarouselItem';

describe('carousel 单元测试', () => {
  test('Carousel 测试', () => {
    const carousel = shallow(
      <Carousel>
        <CarouselItem> CarouselItem1</CarouselItem>
        <CarouselItem> CarouselItem2</CarouselItem>
      </Carousel>);
    expect(carousel.prop('className')).toEqual('br-carousel ');
  });

  test('CarouselItem 测试', () => {
    const carouselItem = shallow(<CarouselItem ></CarouselItem>);
    expect(carouselItem.prop('className')).toEqual('br-carousel__item');
  });

  test('不显示dots且不显示direcrion测试  ', () => {
    const carousel = shallow(
      <Carousel onChange={this.onChange} showDots={false} showArrows={false}>
        <CarouselItem>CarouselItem1</CarouselItem>
        <CarouselItem>CarouselItem2</CarouselItem>
      </Carousel>);
    expect(carousel.prop('children')[1]).toBeFalsy();
  });

  test('轮播子节点数量测试', () => {
    const len = Math.ceil(Math.random() * 10);
    const arr = new Array(len).fill(1);
    const carousel = mount(
      <Carousel onChange={this.onChange} >
        {
          arr.map((v, i) => {
            return <CarouselItem key={i}> CarouselItem1</CarouselItem>;
          })
        }
      </Carousel>);
    expect(carousel.find('.br-carousel__item').length).toBe(len > 1 ? len + 2 : len);
  });
});
