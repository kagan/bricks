import classNames from 'classnames';
import PropTypes, { array } from 'prop-types';
import React from 'react';

export interface IDotsProps {
  length: number;
  activeIndex: number;
  updateCurrentIndex: (index: number) => void;
}

export class Dots extends React.Component<IDotsProps, {}> {
  public render() {
    const {length, activeIndex, updateCurrentIndex} = this.props;
    return (
      <div className="br-carousel__dots">
        {
          new Array(length).fill(0).map((v, i) => {
            return <span key={i} className={`br-carousel__dot_item ${i === (activeIndex % length) ? 'active' : ''}`}
            onClick={() => updateCurrentIndex(length > 1 ? i + 1 : 0)} />;
          })
        }
      </div>
    );
  }
}
