import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

export interface IGridProps {
  /**
   * 默认为false
   * fluid ? 'container' : 'container-fluid'
   */
  fluid?: boolean;
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

export const Grid: React.SFC<IGridProps> = (props: IGridProps) => (
  <div
    className={classNames(props.fluid ? 'container-fluid' : 'container', props.className)}
    style={props.style}
  >{props.children}</div>
);

Grid.propTypes = {
  fluid: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
};
