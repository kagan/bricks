import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { ITree, traverseTree, getTreeLeafs } from '../../utils/utility';
import { IControledDOMProps } from '../../utils/props';
import { Col } from '../col';
import { Icon } from '../icon';
import { Input } from '../input';
import { Row } from '../row';
import { Text } from '../text';
import { VirtualizedTree } from '../tree/VirtualizedTree';
import { AutoSizer, List, ListRowProps } from 'react-virtualized';
import { TreeNode } from '../tree/TreeNode';

const TRANSFER_CONTENT_HEIGHT = 330;
const TITLE_WRAPPER_HEIGHT = 40;
export interface ITransferProps extends IControledDOMProps {
  defaultTreeData?: ITree[];
  treeData: ITree[];
  /**
   * 是否显示过滤框
   */
  filterable?: boolean;
  filterPlaceholder?: string;
  /**
   * 选中或展开状态改变
   */
  onChange?: (treeData: ITree[]) => void;
  /**
   * 自定义过滤规则
   */
  onFilter?: (treeData: ITree[], text: string) => ITree[];
  treeBoxHeader?: React.ReactNode;
  resultBoxHeader?: React.ReactNode;
  resultBoxTitle?: React.ReactNode;
  resultShowParent?: boolean;
  resultShowParentTitle?: boolean;
  style?: React.CSSProperties;
  className?: string;
}

export interface ITransferState {
  filterText?: string;
  filteredTreeData?: ITree[];
}

interface IGetCheckedNodesOptions {
  showParentOnly?: boolean;
  concatParentTitle?: boolean;
}

export class Transfer extends React.Component<ITransferProps, ITransferState> {
  public static propTypes = {
    /**
     * 是否显示过滤框
     */
    filterable: PropTypes.bool,
    filterPlaceholder: PropTypes.string,
    treeBoxHeader: PropTypes.node,
    resultBoxHeader: PropTypes.node,
    resultBoxTitle: PropTypes.string,
    style: PropTypes.object,
    className: PropTypes.string,
  };
  /**
   * 如果传入了外部props，更新到state
   * @param nextProps
   * @param prevState
   */
  public static getDerivedStateFromProps(nextProps: ITransferProps, prevState: ITransferState) {
    if (prevState.filterText) {
      let filteredTreeData = Transfer.filterTreeData(nextProps.treeData, prevState.filterText);
      filteredTreeData = Transfer.copyExpandedState(prevState.filteredTreeData || [], filteredTreeData);
      return {
        ...prevState,
        filteredTreeData,
      };
    }
    return null;
  }
  /**
   * 复制state数据
   * @param fromTreeData
   * @param toTreeData
   */
  public static copyExpandedState(fromTreeData: ITree[], toTreeData: ITree[]): ITree[] {
    const expandState: { [key: string]: boolean | undefined } = {};
    fromTreeData.forEach((treeNode) => {
      traverseTree(treeNode, (node) => {
        if (!Transfer.isLeaf(node)) {
          expandState[node.key] = node.expanded;
        }
      });
    });
    return Transfer.treeDataMap(toTreeData, (node) => {
      node.expanded = expandState[node.key];
      return node;
    });
  }

  public static treeDataMap(treeData: ITree[], func: (node: ITree) => ITree): ITree[] {
    return treeData.map((node) => {
      if (node.children && node.children.length) {
        return func({
          ...node,
          children: Transfer.treeDataMap(node.children, func),
        });
      }
      return func(node);
    });
  }
  /**
   * 选中状态
   * @param treeNode
   */
  public static isChecked(treeNode: ITree) {
    if (Transfer.isLeaf(treeNode)) {
      return treeNode.checked;
    }
    // 根据子节点是否全选判断父节点的状态
    return !(treeNode.children || []).find((child) => !child.checked);
  }

  /**
   * 半选状态
   * @param treeNode
   */
  public static isHalfChecked(treeNode: ITree) {
    const { children = [] } = treeNode;
    return children.some((child) => !child.checked) && children.some((child) => !!child.checked);
  }

  /**
   * 叶子节点
   * @param treeNode
   */
  public static isLeaf(treeNode: ITree) {
    return !treeNode.children || !treeNode.children.length;
  }

  /**
   *  filter的时候数据复制一份
   * @param treeData
   * @param text
   */
  public static filterTreeData(treeData: ITree[], text: string): ITree[] {
    const filterTreeData = treeData.map((node) => {
      if (node.title.includes(text)) {
        return { ...node };
      }
      if (node.children) {
        const matchedChildren = Transfer.filterTreeData(node.children, text);
        if (matchedChildren.length) {
          return {
            ...node,
            checked: !matchedChildren.find((childNode) => !childNode.checked),
            expanded: true,
            children: matchedChildren,
          };
        }
      }
      return null;
    });

    return filterTreeData.filter((node) => node !== null) as ITree[];
  }

  public tree = React.createRef<VirtualizedTree>();

  public list = React.createRef<List>();

  constructor(props: ITransferProps) {
    super(props);
    this.handleInputChange = this.handleInputChange.bind(this);
    this.handleTreeChange = this.handleTreeChange.bind(this);
    this.renderResultBox = this.renderResultBox.bind(this);
    this.setNodeUnchecked = this.setNodeUnchecked.bind(this);
    this.resultRenderer = this.resultRenderer.bind(this);
    this.recomputeRowHeights = this.recomputeRowHeights.bind(this);
    this.state = {
      filterText: '',
    };
  }

  public componentDidUpdate() {
    // 每次更新完都重新计算高度，否则节点会重合
    this.recomputeRowHeights();
  }

  /** 计算高度 */
  public recomputeRowHeights() {
    if (this.list.current) {
      this.list.current.recomputeRowHeights();
    }
  }
  /**
   * 搜索框输入事件
   * @param event
   */
  public handleInputChange(event: React.ChangeEvent<HTMLInputElement>) {
    const text = event.target.value;
    if (!text || !text.trim()) {
      this.setState({
        filteredTreeData: undefined,
        filterText: '',
      });
      // 清空文字时，复制展开状态到原始数据中
      if (this.state.filteredTreeData && this.props.onChange) {
        this.props.onChange(Transfer.copyExpandedState(this.state.filteredTreeData, this.props.treeData));
      }
    } else {
      this.setState({ filterText: text, filteredTreeData: Transfer.filterTreeData(this.props.treeData, text) });
    }
  }
  /**
   * 点击事件
   * @param treeData
   */
  public handleTreeChange(treeData: ITree[]) {
    let newTreeData = treeData;
    if (this.state.filteredTreeData) {
      newTreeData = this.copyCheckedState(treeData, this.props.treeData);
      this.setState({ filteredTreeData: treeData });
    }
    if (this.props.onChange) {
      this.props.onChange(newTreeData);
    }
  }
  public copyCheckedState(fromTreeData: ITree[], toTreeData: ITree[]) {
    const leafs: { [key: string]: ITree } = {};
    fromTreeData.forEach((tree) => {
      traverseTree(tree, (node) => {
        if (!node.children || !node.children.length) {
          leafs[node.key] = node;
        }
      });
    });
    return Transfer.treeDataMap(toTreeData, (node) => {
      const leaf = leafs[node.key];
      if (leaf) {
        return {
          ...node,
          checked: leaf.checked,
        };
      }

      if (node.children && node.children.length) {
        return {
          ...node,
          checked: !node.children.find((child) => !child.checked),
        };
      }
      return node;
    });
  }

  /**
   * 点击关闭图标事件
   * @param treeNode
   */
  public setNodeUnchecked(treeNode: ITree) {
    if (!treeNode.disabled) {
      const newTreeData = this.updateCheckState(this.props.treeData, treeNode.key, false);
      let newFilteredData;
      if (this.state.filteredTreeData) {
        newFilteredData = this.updateCheckState(this.state.filteredTreeData, treeNode.key, false);
        this.setState({ filteredTreeData: newFilteredData });
      }
      if (this.props.onChange) {
        this.props.onChange(newTreeData);
      }
    }
  }
  /**
   * 通过右边的结果框控制树结构的选择状态
   * @param treeData
   * @param key
   * @param checked
   */
  public updateCheckState(treeData: ITree[], key: string, checked: boolean, parentHalfChecked?: boolean) {
    return Transfer.treeDataMap(treeData, (node) => {
      if (node.key === key) {
        if (node.children) {
          // treeMap有点冗余，需要精简下
          node.children = Transfer.treeDataMap(node.children, (childNode) => {
            return {
              ...childNode,
              checked,
            };
          });
        }
        return {
          ...node,
          checked,
        };
      }
      if (!Transfer.isLeaf(node)) {
        return {
          ...node,
          checked: Transfer.isChecked(node),
          halfChecked: Transfer.isHalfChecked(node),
        };
      }
      return node;
    });
  }
  /**
   * 结果框样式
   * @param param0
   */
  public resultRenderer({ index, key, style }: ListRowProps) {
    const checkedNodes = this.getTreeDataCheckedResults(this.props.treeData)[index];
    return (
      <li
        style={style}
        key={'result-' + key}
        className="br-transfer__resultBorder clearfix"
      >
        <Text ellipsis color={checkedNodes.disabled ? '#a8a8a8' : ''}>{checkedNodes.title}</Text>
        <Icon
          className={checkedNodes.disabled ? 'br-transfer__result-icon disabled' : 'br-transfer__result-icon'}
          type="close-solid"
          color={checkedNodes.disabled ? '#b8b8b8' : '#e22e33'}
          onClick={() => this.setNodeUnchecked(checkedNodes)}
        />
      </li>
    );
  }

  /**
   * 渲染结果框
   */
  public renderResultBox() {
    const rowCount = this.getTreeDataCheckedResults(this.props.treeData).length;
    return (
      <AutoSizer >
        {
          ({ height, width }) => (
            <List
              height={height}
              width={width}
              ref={this.list}
              rowHeight={TreeNode.TREE_NODE_HEIGHT}
              rowCount={rowCount}
              rowRenderer={this.resultRenderer}
              overscanRowCount={20}
              style={{ padding: '5px 8px', outline: 0 }}
            />
          )
        }
      </AutoSizer>
    );
  }
  /**
   * 获取选中的数据
   * @param treeData
   */
  public getTreeDataCheckedResults(treeData: ITree[]) {
    const results: ITree[] = [];
    treeData.forEach((tree) => {
      const isLeaf = !tree.children || !tree.children.length;
      const treeLeafs = getTreeLeafs(tree);
      const hasLeafDisabled = treeLeafs.some((node) => !!node.disabled && !node.checked);
      if (tree.checked && !hasLeafDisabled) {
        if (this.props.resultShowParent || isLeaf) {
          results.push(tree);
          return;
        }
      }
      // 非叶子节点
      if (!isLeaf) {
        let children = tree.children || [];
        if (this.props.resultShowParentTitle) {
          children = children.map((subTree) => ({ ...subTree, title: `${tree.title}/${subTree.title}` }));
        }
        results.push(...this.getTreeDataCheckedResults(children));
      }
    });
    return results;
  }

  public render() {
    const {
      className, style, treeBoxHeader, resultBoxHeader, filterable, filterPlaceholder,
      resultBoxTitle, treeData, onChange, resultShowParent, resultShowParentTitle, ...domProps
    } = this.props;
    /** 左框的高度 */
    const treeHeight = filterable ? TRANSFER_CONTENT_HEIGHT - 40 : TRANSFER_CONTENT_HEIGHT;
    /** 右框的高度 */
    const resultHeight = resultBoxTitle ? TRANSFER_CONTENT_HEIGHT - 40 : TRANSFER_CONTENT_HEIGHT;
    return (
      <div
        {...domProps as IControledDOMProps}
        className={classNames('br-transfer', className)}
        style={style}
      >
        <Row className="no-margin">
          <Col xs={5} className="no-padding">{treeBoxHeader}</Col>
          <Col xs={2} className="no-padding"></Col>
          <Col xs={5} className="no-padding">{resultBoxHeader}</Col>
        </Row>
        <Row className="br-transfer__content no-margin">
          <Col xs={5} className="no-padding" style={{ border: '1px solid #d4d4d4' }}>
            {
              filterable &&
              (
                <div className="br-transfer__input-wrapper">
                  <Input
                    placeholder={filterPlaceholder || ''}
                    onChange={this.handleInputChange}
                    icon={<Icon type="search" />} />
                </div>
              )
            }
            <div style={{ height: treeHeight }}>
              <VirtualizedTree
                onChange={this.handleTreeChange}
                treeData={this.state.filteredTreeData || treeData}
              />
            </div>
          </Col>
          <Col xs={2} className="br-transfer__arrow-col no-padding">
            <Icon type="transfer" color="#a4a4a4" size={20} />
          </Col>
          <Col xs={5} className="no-padding" style={{ border: '1px solid #d4d4d4' }}>
            {
              resultBoxTitle &&
              <div className="br-transfer__result-title">{resultBoxTitle}</div>
            }
            <div style={{ height: resultHeight }}>
              {this.renderResultBox()}
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}
