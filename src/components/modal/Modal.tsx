import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';
import { Backdrop } from '../backdrop';
import { Icon } from '../icon';
import Portal from '../portal/Portal';
import { Fade } from '../fade';

export interface IModalProps {
  style?: React.CSSProperties;
  className?: string;
  showBackdrop?: boolean;
  onBackdropClick?: () => void;
  visible: boolean;
  children?: React.ReactNode;
}

export interface IModalHeaderProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
  onClose?: () => void;
}

export interface IModalContentProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

export interface IModalFooterProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

const Header = (props: IModalHeaderProps) => {
  return (
    <div className={classNames('br-modal__header', props.className)} style={props.style}>
      <div className="br-modal__header--title">
        {props.children}
      </div>
      <Icon className="br-modal__close-icon" type="close" onClick={props.onClose} />
    </div>
  );
};

const Content = (props: IModalContentProps) => {
  return (
    <div className={classNames('br-modal__content', props.className)} style={props.style}>
      {props.children}
    </div>
  );
};

const Footer = (props: IModalFooterProps) => {
  return (
    <div className={classNames('br-modal__footer', props.className)} style={props.style}>
      {props.children}
    </div>
  );
};

export class Modal extends React.Component<IModalProps> {
  public static Header = Header;
  public static Content = Content;
  public static Footer = Footer;

  public static propTypes = {
    style: PropTypes.object,
    className: PropTypes.string,
    showBackdrop: PropTypes.bool,
    onBackdropClick: PropTypes.func,
    visible: PropTypes.bool,
    children: PropTypes.node,
  };

  public static defaultProps = {
    showBackdrop: true,
  };

  public isBodyOverflowing = false;
  public scrollBarSize = 0;

  constructor(props: IModalProps) {
    super(props);
    // this.
  }

  public render() {
    const { style, className, showBackdrop, onBackdropClick, visible, children } = this.props;

    return (
      <Portal>
        {showBackdrop ? <Backdrop visible={visible} onBackdropClick={onBackdropClick} /> : null}
        <Fade
          active={visible}
          className={classNames('br-modal', className)}
          style={style}
        >
          {children}
        </Fade>
      </Portal>
    );
  }
}
