import { shallow, mount } from 'enzyme';
import React from 'react';
import { Confirm } from '../Confirm';

describe('Confirm 组件测试', () => {
  test('confirm 测试', () => {
    const confirm = shallow(<Confirm ></Confirm>);
    expect(confirm.find('span.br-confirm__title-bar').prop('className')).toEqual('br-confirm__title-bar');
    expect(confirm.find('div.br-confirm__body').prop('className')).toEqual('br-confirm__body');
    expect(confirm.find('div.br-confirm__footer').prop('className')).toEqual('br-confirm__footer');
  });

  test('点击测试', () => {
    const onClose = jest.fn();
    const onOk = jest.fn();
    const onCancel = jest.fn();
    const confirm = mount(<Confirm onClose={onClose} onOk={onOk} onCancel={onCancel}></Confirm>);
    confirm.find('i.icon.icon-close.br-modal__close-icon').simulate('click');
    confirm.find('button.br-confirm__ok-button').simulate('click');
    confirm.find('button.button-outline').simulate('click');
    expect(onOk.mock.calls.length).toBe(1);
    expect(onCancel.mock.calls.length).toBe(1);
    expect(onClose.mock.calls.length).toBe(1);
  });
});
