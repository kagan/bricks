import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

import { Icon } from '../icon';

import { IFormControlProps } from './FormControl';

export interface IFormHelpProps extends IFormControlProps, IDOMProps {
  icon?: string;
}

export const FormHelp: React.SFC<IFormHelpProps> =
  ({ children, className, style, icon, success, warn, error, ...domProps }: IFormHelpProps) => {
    const classes = {
      'br-form-help': true,
      'br-form-help--error': error,
      'br-form-help--warn': warn,
      'br-form-help--success': success,
      'br-form-help--with-icon': !!icon,
    };

    return (
      <div
        {...domProps as IDOMProps}
        className={classNames(classes, className)}
        style={style}
      >
        {icon && <Icon className="br-form-help__icon" type={icon} />}
        <span className="br-form-help__content">{children}</span>
      </div>
    );
  };

FormHelp.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
  style: PropTypes.object,
  icon: PropTypes.string,
  success: PropTypes.bool,
  warn: PropTypes.bool,
  error: PropTypes.bool,
};
