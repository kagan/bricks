import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IFormLabelProps extends IDOMProps {
  children?: React.ReactNode;
  align?: 'left' | 'center' | 'right';
  className?: string;
}

export const FormLabel: React.SFC<IFormLabelProps> = (props: IFormLabelProps) => {
  const { className = '', children, align = 'left', ...domProps } = props;
  return (
    <label className={classNames('br-form-label', `align-${align}`, className)}
      {...domProps as IDOMProps}
    >
      <span className="br-form-label__content">{children}</span>
    </label>
  );
};

FormLabel.propTypes = {
  children: PropTypes.node,
  align: PropTypes.oneOf(['left', 'center', 'right']),
};
