import React, { CSSProperties, ReactNode } from 'react';
import { Icon } from '../icon/Icon';
import classNames from 'classnames';

import { IDOMProps } from '../../utils/props';
interface IAlertProps extends IDOMProps {
  /** 组件类型 */
  type?: string;
  /** 辅助性文字 */
  description?: ReactNode;
  /** 标题文字 */
  message?: ReactNode;
  /** 是否显示图标 */
  showIcon?: boolean;
  /** 是否显示关闭按钮 */
  closable?: boolean;
  /** 关闭按钮点击事件 */
  onClose?: () => void;
  /** 图标自定义 */
  icon?: ReactNode;
  /** 是否展示 */
  show?: boolean;
  /** 外部控制className */
  className?: string;
  /** 外部控制样式 */
  style?: CSSProperties;
}

/**
 * 图标样式
 * @param param0
 */
function AlertIcon({ icon, type, description }: IAlertProps) {
  /** 大小 */
  const iconSize = description ? 'br-alert__icon--large' : 'br-alert__icon--normal';
  /** 样式 */
  let iconType = '';
  /** 颜色 */
  let iconColor = '';
  switch (type) {
    case 'success':
      iconType = description ? 'check-circle-o' : 'check-circle';
      iconColor = 'br-alert__icon--success';
      break;
    case 'info':
      iconType = description ? 'info-solid-o' : 'info-solid';
      iconColor = 'br-alert__icon--info';
      break;
    case 'warning':
      iconType = description ? 'warn-circle-o' : 'warn-circle';
      iconColor = 'br-alert__icon--warning';
      break;
    case 'error':
      iconType = description ? 'close-solid-o' : 'close-solid';
      iconColor = 'br-alert__icon--error';
      break;
  }
  return (
    <div className={classNames('br-alert__icon', iconColor, iconSize)}>
      {icon ? icon : <Icon type={iconType}></Icon>}
    </div>
  );
}

/** 关闭按钮 */
function CloseIcon({ onClose }: IAlertProps) {
  return (
    // <div className="br-alert__close">
    <Icon type="close-thin" onClick={onClose} className="br-alert__close" />
    // </div>
  );
}

export const Alert: React.SFC<IAlertProps> = (props: IAlertProps) => {
  const { className, style, ...domProps } = props;
  /**
   * 组件大小
   */
  const alertClassName = props.description ? 'br-alert--large' : 'br-alert--normal';
  /**
   * description 的样式
   */
  const descriptionClassName = props.description ? 'br-alert--large__description' : '';
  /**
   * 标题大小
   */
  const titleClassName = props.description ? 'br-alert--large__title' : 'br-alert--normal__title';
  /**
   * 是否包含图标
   */
  const wetherIcon = props.showIcon ? 'right' : '';
  /**
   * 背景颜色
   */
  const typeClassName = {
    'br-alert--success': props.type === 'success',
    'br-alert--info': props.type === 'info',
    'br-alert--warning': props.type === 'warning',
    'br-alert--error': props.type === 'error',
  };
  return (
    <div
      {...domProps as IDOMProps}
      className={classNames('br-alert ', alertClassName, typeClassName, className)}
      style={style}
    >
      {
        props.showIcon && <AlertIcon
          icon={props.icon}
          type={props.type}
          description={props.description} />
      }
      <div className={titleClassName}>{props.message}</div>
      {props.closable && <CloseIcon onClose={props.onClose} />}
      <div className={classNames(descriptionClassName, wetherIcon)}> {props.description}</div>
    </div>
  );

};
