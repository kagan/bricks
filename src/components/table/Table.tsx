import PropTypes from 'prop-types';
import React from 'react';
import RcTable, { IColumnProps, IRcTableProps } from 'rc-table';
import classNames from 'classnames';

export interface IRcTableProps {
  data: object[]; // data record array to be rendered
  columns: IColumnProps[]; // The columns config of table, see table below
  type?: string; // default 'vertical' ,
  prefixCls?: string;	// default rc-table
  className?: string; // additional className
  id?: string; // identifier of the container div
  useFixedHeader?: boolean;	// default false,whether use separator table for header. better set width for columns
  scroll?: object;
  // default {x: false, y: false},whether table can be scroll in x/y direction,
  // `x` or `y` can be a number that indicated the width and height of table body
  expandIconAsCell?: boolean;	// default false,whether render expandIcon as a cell
  expandIconColumnIndex?: number;
  // default 0,The index of expandIcon which column will be inserted when expandIconAsCell is false
  rowKey?: string | ((record: any) => string);
  rowClassName?: string | ((record: any, index: number, indent: number) => string); // get row's className
  rowRef?: (record: any, index: number, indent: number) => string; // get row's ref key
  defaultExpandedRowKeys?: string[]; // default []	initial expanded rows keys
  expandedRowKeys?: string[]; // current expanded rows keys
  defaultExpandAllRows?: boolean; // default false,Expand All Rows initially
  onExpandedRowsChange?: (expandedRows: any) => void;
  // save the expanded rows in the internal state	function to call when the expanded rows change
  onExpand?: (expanded: any, record: any) => void;
  // function to call when click expand icon
  expandedRowClassName?: (record: any, index: number, indent: number) => string;
  // get expanded row's className
  expandedRowRender?: (record: any, index: number, indent: number, expanded: any) => React.ReactNode;
  // Content render to expanded row
  indentSize?: number;
  onRow?: (record: any, index: number) => void;
  onHeaderRow?: (record: any, index: number) => void; // Set custom props per each header row.
  showHeader?: boolean;	 // default true,whether table head is shown
  title?: (currentData: any) => void; // table title render function
  footer?: (currentData: any) => void; // table footer render function
  emptyText?: React.ReactNode | (() => void); // `No Data`	Display text when data is empty
  components?: object;
  // Override table elements, see [#171](https://github.com/react-component/table/pull/171) for more details
}

export interface IColumnProps {
  key?: string; // key of this column
  className?: string; // className of this column
  colSpan?: number; // thead colSpan of this column
  title?: React.ReactNode; // title of this column
  dataIndex?: string; // display field of the data record
  width?: string | number; // width of the specific proportion calculation according to the width of the columns
  fixed?: string | boolean; // this column will be fixed when table scroll horizontally: true or 'left' or 'right'
  align?: string; // specify how content is aligned
  onCell?: (record: any) => void; // Set custom props per each cell.
  onHeaderCell?: (record: any) => void; // Set custom props per each header cell.
  render?: (value: any, row: any, index: number) => React.ReactNode;
  // The render function of cell, has three params: the text of this cell,
  // the record of this row, the index of this row,
  // it's return an object:{ children: value,
  // props: { colSpan: 1, rowSpan:1 } } ==> 'children' is the text of this cell,
  // props is some setting of this cell, eg: 'colspan' set td colspan, 'rowspan' set td rowspan
  // onCellClick[deprecated]	Function(row, event)		Called when column's cell is clicked
}

export interface ITableProps extends IRcTableProps {
  hideColumnLine?: boolean;
  showVerticalBorder?: boolean;
}

export const Table: React.SFC<ITableProps> = (props: ITableProps) => {
  let className = '';
  if (!props.hideColumnLine) {
    className = 'br-table--no-vertical-border';
  }

  if (props.showVerticalBorder) {
    className = '';
  }

  return <RcTable
    prefixCls="br-table"
    {...props}
    className={classNames(className, props.className)}
  />;
};

Table.defaultProps = {
  hideColumnLine: true,
  showVerticalBorder: false,
  emptyText: '暂无数据',
};

Table.propTypes = {
  hideColumnLine: PropTypes.bool,
  showVerticalBorder: PropTypes.bool,
  emptyText: PropTypes.string,
};
