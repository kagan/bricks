import { shallow, mount } from 'enzyme';
import React from 'react';
import { Checkbox } from '../Checkbox';

describe('checkbox单元测试', () => {
  test('checkbox', () => {
    const checkbox = shallow(<Checkbox></Checkbox>);
    expect(checkbox.prop('className')).toEqual('br-checkbox');
  });

  test('被选中', () => {
    const checkbox = shallow(<Checkbox checked></Checkbox>);
    expect(checkbox.prop('className')).toEqual('br-checkbox br-checkbox--checked');
  });

  test('模拟点击', () => {
    const wrapper = mount(<Checkbox/>);
    expect(wrapper.state('checked')).toBe(false);
    wrapper.find('input').simulate('change', { target: { checked: true } });
    expect(wrapper.state('checked')).toBe(true);
  });

  test('受控', () => {
    const wrapper = mount(<Checkbox disabled/>);
    expect(wrapper.state('checked')).toBe(false);
    wrapper.find('input').simulate('change');
    expect(wrapper.state('checked')).toBe(false);
  });
});
