import PropTypes from 'prop-types';
import React from 'react';

export interface IRowProps {
  className?: string;
  style?: React.CSSProperties;
  children?: React.ReactNode;
}

export const Row: React.SFC<IRowProps> = (props: IRowProps) => {
  const className = props.className || '';
  const classString = `row ${className}`;
  return <div className={classString} style={props.style}>{props.children}</div>;
};

Row.propTypes = {
  className: PropTypes.string,
  style: PropTypes.object,
  children: PropTypes.node,
};
