import classNames from 'classnames';
import PropTypes from 'prop-types';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface IButtonProps extends IDOMProps {
  onClick?: React.MouseEventHandler;
  // 类型
  primary?: boolean;
  secondary?: boolean;
  outline?: boolean;
  dashed?: boolean;
  link?: boolean;

  // 形状
  round?: boolean;
  noRadius?: boolean;
  fat?: boolean;

  // 尺寸
  xLarge?: boolean;
  large?: boolean;
  small?: boolean;
  xSmall?: boolean;

  // 宽度
  long?: boolean;
  short?: boolean;

  // 颜色
  success?: boolean;
  warn?: boolean;

  // 禁用状态
  disabled?: boolean;

  href?: string;

  className?: string;
  style?: React.CSSProperties;

  children?: React.ReactNode;
}

export const Button: React.SFC<IButtonProps> = (props: IButtonProps) => {

  const {
    className: tempClassName, style, onClick, children, primary, secondary,
    outline, dashed, link, round, noRadius, fat, xLarge, large, small, xSmall,
    long, short, href, success, warn, disabled, ...domProps
  } = props;
  const className = classNames({
    'button': true,
    'button-primary': primary,
    'button-secondary': secondary,
    'button-outline': outline,
    'button-dashed': dashed,
    'button-text': link,
    'button-round': round,
    'button-rectangle': noRadius,
    'button-fat': fat,
    'button-xl': xLarge,
    'button-lg': large,
    'button-sm': small,
    'button-xs': xSmall,
    'button-long': long,
    'button-short': short,
    'button-success': success,
    'button-warn': warn,
    'button-disabled': disabled,
  }, tempClassName);
  if (href) {
    return (
      <a
        {...domProps as IDOMProps}
        className={className}
        style={style}
        onClick={onClick}
        href={href}
      >
        {children}
      </a>
    );
  } else {
    return (
      <button
        {...domProps as IDOMProps}
        className={className}
        style={style}
        onClick={onClick}
        disabled={disabled}
      >
        {children}
      </button>
    );
  }
};

Button.propTypes = {
  onClick: PropTypes.func,
  // 类型
  primary: PropTypes.bool,
  secondary: PropTypes.bool,
  outline: PropTypes.bool,
  dashed: PropTypes.bool,
  link: PropTypes.bool,

  // 形状
  round: PropTypes.bool,
  noRadius: PropTypes.bool,
  fat: PropTypes.bool,

  // 尺寸
  xLarge: PropTypes.bool,
  large: PropTypes.bool,
  small: PropTypes.bool,
  xSmall: PropTypes.bool,

  // 宽度
  long: PropTypes.bool,
  short: PropTypes.bool,

  // 颜色
  success: PropTypes.bool,
  warn: PropTypes.bool,

  // 禁用状态
  disabled: PropTypes.bool,

  href: PropTypes.string,
};
