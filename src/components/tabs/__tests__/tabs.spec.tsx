import { shallow, mount } from 'enzyme';
import React from 'react';
import { Tabs } from '../Tabs';
import { TabContainer } from '../TabContainer';
import { Tab } from '../Tab';

describe('Tabs 测试', () => {
  test('Tabs 显示', () => {
    const onTabsClick = jest.fn();
    const wrapper = shallow(<Tabs onChange={onTabsClick} />);
    expect(wrapper.prop('className')).toEqual('br-tabs');
  });

  test('Tabs下的Tab子组件展示', () => {
    const onTabsClick = jest.fn(() => { wrapper.setProps({ activeKey: 'b' }); });
    const wrapper = mount(
      <Tabs onChange={onTabsClick} className="br-tabs" >
        <Tab value="a" className="AA">test1</Tab>
        <Tab value="b" className="BB">test2</Tab>
      </Tabs>);
    expect(wrapper.prop('className')).toEqual('br-tabs');
    /** Tab展示 */
    expect(wrapper.find('ul').children()).toHaveLength(2);
    expect(wrapper.find('ul').childAt(0).type()).toEqual('li');
    expect(wrapper.find('li')).toHaveLength(2);
    expect(wrapper.find(Tab).at(0).props().value).toEqual('a');
    expect(wrapper.find(Tab).at(1).props().value).toEqual('b');
    expect(wrapper.containsAllMatchingElements([
      <span >test1</span>,
      <span >test2</span>,
    ])).toEqual(true);
  });

  test('模拟点击，点击‘test2’之后的效果', () => {
    const onTabsClick = jest.fn(() => {
      wrapper.setProps({ activeKey: 'b' });
    });
    const wrapper = mount(
      <Tabs onChange={onTabsClick} className="br-tabs" >
        <Tab value="a" className="AA">test1</Tab>
        <Tab value="b" className="BB">test2</Tab>
      </Tabs>,
    );
    wrapper.find('span.BB').simulate('click');
    expect(wrapper.find('ul').childAt(1).find('span').hasClass('tab-item active BB')).toEqual(true);
  });
});

describe('Tab 测试', () => {
  test('Tab 显示', () => {
    const accordion = shallow(<Tab value="a" />);
    expect(accordion.prop('className')).toEqual('tab-item');
  });

  test('Tab disabled效果', () => {
    const accordion = shallow(<Tab value="a" disabled />);
    expect(accordion.prop('className')).toEqual('tab-item tab-item--disabled');
  });
});

describe('TabContainer 测试', () => {
  test('TabContainer 展示', () => {
    const accordion = shallow(<TabContainer />);
    expect(accordion.prop('className')).toEqual('br-tabs__panel');
  });
});
