import React from 'react';
import classNames from 'classnames';

import { IControledDOMProps } from '../../utils/props';

export interface ITabItemProps {
  /**
   * Tab value
   */
  value: string;
  /**
   * 激活
   */
  active?: boolean;
  disabled?: boolean;
  onChange?: (value: string) => void;
  className?: string;
  style?: React.CSSProperties;
}

export class Tab extends React.Component<ITabItemProps, {}> {
  constructor(props: ITabItemProps) {
    super(props);
    this.onToggle = this.onToggle.bind(this);
  }

  public onToggle() {
    if (!this.props.disabled && this.props.onChange) {
      this.props.onChange(this.props.value);
    }
  }

  public render() {
    const { className, style, disabled, active, ...domProps } = this.props;
    return (
      <span
        {...domProps as IControledDOMProps}
        className={classNames(
          'tab-item',
          active ? 'active' : '',
          { 'tab-item--disabled': disabled },
          className,
        )}
        style={style}
        onClick={this.onToggle}
      >
        {this.props.children}
      </span>);
  }
}

/**
 * <Tab value="tab1" active={false}>按钮</Tab>
 */
