import React from 'react';
import classNames from 'classnames';

import { IDOMProps } from '../../utils/props';

interface ITabContainerProps extends IDOMProps {
  className?: string;
  style?: React.CSSProperties;
}

export class TabContainer extends React.Component<ITabContainerProps, {}> {
  public render() {
    const { className, style, children, ...domProps } = this.props;
    return (
      <div
        {...domProps as IDOMProps}
        className={classNames('br-tabs__panel', className)}
        style={style}
      >
        {this.props.children}
      </div>
    );
  }
}
