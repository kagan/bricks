import classNames from 'classnames';
import React from 'react';

import { IDOMProps } from '../../utils/props';

export interface ILineProgressProps {
  className?: string;
  style?: React.CSSProperties;
  /** 进度条百分比 */
  percent?: number;
  /** 进度条宽度 */
  strokeWidth?: number;
  /** 进度条颜色 */
  strokeColor?: string;
  /** 进度条轨迹填充色 */
  trailColor?: string;
  /** 进度条边缘形状, round:圆角 square：方角 */
  strokeLinecap?: 'round' | 'square';
  /** 是否显示进度数值 */
  format?: (percent: number) => (React.ReactNode | string);
  /** 是否显示进度数值 */
  showText?: boolean;
}

export const LineProgress: React.SFC<ILineProgressProps> = (props: ILineProgressProps) => {
  const { className = '', style = {}, strokeWidth = 10, strokeColor = '#00b7fe', trailColor = '#fafafa',
    strokeLinecap = 'round', showText = true, percent = 0, format, ...domProps } = props;
  const borderRadius = strokeLinecap === 'square' ? 0 : strokeWidth / 2;
  return (
    <div
      {...domProps as IDOMProps}
      className={classNames('br-progress br-progress__line', className)}
      style={style}
    >
      <div className={`br-progress__line-trail ${showText ? '' : 'br-progress__line-full'}`}
        style={{
          height: `${strokeWidth}px`,
          borderRadius,
          backgroundColor: trailColor,
        }}>
        <div className={classNames('br-progress__line-path')}
          style={{
            width: `${Math.min(percent, 100)}%`,
            height: `${strokeWidth}px`,
            borderRadius,
            backgroundColor: strokeColor,
          }} />
      </div>
      {
        showText &&
        <span className="br-progress__line-text">{format ? format(percent) : `${percent}%`}</span>
      }
    </div>
  );
};
