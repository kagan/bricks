import { mount } from 'enzyme';
import React from 'react';
import { Progress } from '../Progress';

describe('Progress 进度条测试', () => {
  beforeAll(() => {
    jest.useFakeTimers();
  });

  afterAll(() => {
    jest.useRealTimers();
  });

  test('Progress className测试', () => {
    const className = 'myClassName';
    const lineProgress = mount(<Progress className={className} />);
    const circleProgress = mount(<Progress type="circle" className={className} />);
    expect(lineProgress.hasClass(className)).toBeTruthy();
    expect(circleProgress.hasClass(className)).toBeTruthy();
  });

  test('Progress 进度条类型测试', () => {
    const lineProgress = mount(<Progress percent={30} />);
    const circleProgress = mount(<Progress percent={30} type="circle" />);
    expect(lineProgress.find('.br-progress').props().className).toMatch('br-progress__line');
    expect(circleProgress.find('.br-progress').props().className).toMatch('br-progress__circle');
  });

  test('Progress 进度条边缘形状测试', () => {
    const strokeLinecap = 'square';
    const lineProgress = mount(<Progress percent={30} strokeLinecap={strokeLinecap} />);
    const circleProgress = mount(<Progress percent={30} type="circle" strokeLinecap={strokeLinecap} />);
    expect(lineProgress.find('.br-progress__line-trail').props().style.borderRadius).toBe(0);
    expect(lineProgress.find('.br-progress__line-path').props().style.borderRadius).toBe(0);
    expect(circleProgress.find('.br-progress__circle-path').props().strokeLinecap).toBe(strokeLinecap);
  });

  test('Progress 进度条颜色测试', () => {
    const trailColor = '#fff';
    const strokeColor = '#f00';
    const props = {
      percent: 30,
      trailColor,
      strokeColor,
    };
    const lineProgress = mount(<Progress {...props} />);
    const circleProgress = mount(<Progress {...props} type="circle" />);
    expect(lineProgress.find('.br-progress__line-trail').props().style.backgroundColor).toBe(trailColor);
    expect(lineProgress.find('.br-progress__line-path').props().style.backgroundColor).toBe(strokeColor);
    expect(circleProgress.find('.br-progress__circle-trail').props().stroke).toBe(trailColor);
    expect(circleProgress.find('.br-progress__circle-path').props().stroke).toBe(strokeColor);
  });

  test('Progress 进度条不显示文案内容测试', () => {
    const showText = false;
    const lineProgress = mount(<Progress percent={30} showText={showText} />);
    const circleProgress = mount(<Progress percent={30} type="circle" showText={showText} />);
    expect(lineProgress.find('.br-progress__line-trail').props().className).toMatch('br-progress__line-full');
    expect(circleProgress.find('.br-progress__circle-text').length).toBe(0);
  });

  test('Progress 进度条文案内容自定义测试', () => {
    const textTail = 'percent';
    const percent = 30;
    const formatFuc = jest.fn((value) => value + textTail);
    const lineProgress = mount(<Progress percent={percent} format={formatFuc} />);
    const circleProgress = mount(<Progress percent={percent} type="circle" format={formatFuc} />);
    expect(lineProgress.find('.br-progress__line-text').text()).toBe(percent + textTail);
    expect(circleProgress.find('.br-progress__circle-text').text()).toBe(percent + textTail);
  });

  test('Progress 进度条宽度测试', () => {
    const strokeWidth = 12;
    const percent = Math.ceil(Math.random() * 100);
    const lineProgress = mount(<Progress percent={percent} strokeWidth={strokeWidth} />);
    const circleProgress = mount(<Progress percent={percent} type="circle" strokeWidth={strokeWidth} />);
    expect(lineProgress.find('.br-progress__line-trail').props().style.height).toBe(strokeWidth + 'px');
    expect(lineProgress.find('.br-progress__line-path').props().style.height).toBe(strokeWidth + 'px');
    expect(lineProgress.find('.br-progress__line-path').props().style.width).toBe(percent + '%');
    expect(circleProgress.find('.br-progress__circle-trail').props().strokeWidth).toBe(strokeWidth);
    expect(circleProgress.find('.br-progress__circle-path').props().strokeWidth).toBe(strokeWidth);
  });

  test('Progress 环形进度条开始角度测试', () => {
    const startDegree = 30;
    const percent = Math.floor(Math.random() * 100);
    const circleProgress = mount(<Progress percent={percent} type="circle" startDegree={startDegree} />);
    expect(circleProgress.find('svg').props().style.transform).toBe(`rotate(${startDegree}deg)`);
  });

  test('Progress 环形进度条percent为0左右测试', () => {
    const circleProgress = mount(<Progress type="circle" percent={10} />);
    circleProgress.setProps({percent: 0});
    circleProgress.setProps({percent: 50});
    jest.runAllTimers();
    circleProgress.setProps({percent: 0});
    jest.runAllTimers();
    circleProgress.unmount();
  });
});
