import PropTypes from 'prop-types';
import RcPagination from 'rc-pagination';
import React from 'react';

export interface IPropagationProps {
  total: number;
  pageSize?: number;
  defaultCurrent?: number;
  showQuickJumper?: boolean;
  showLessItems?: boolean;
  className?: string;
  style?: React.CSSProperties;
  align?: 'center' | 'left' | 'right';
  onChange?: (current: number, pageSize: number) => void;
  current?: number;
}

// TODO: 输入框默认值
export const Pagination: React.SFC<IPropagationProps> = (props: IPropagationProps) => {
  const totalPage = (props.total || 0) / (props.pageSize || 10);
  const locale = {
    // Options.jsx
    items_per_page: '条/页',
    jump_to: `共${Math.ceil(totalPage)}页，到第`,
    jump_to_confirm: '确定',
    page: '页',

    // Pagination.jsx
    prev_page: '上一页',
    next_page: '下一页',
    prev_5: '向前 5 页',
    next_5: '向后 5 页',
    prev_3: '向前 3 页',
    next_3: '向后 3 页',
  };
  let showQuickJumper: any = false;
  if (props.showQuickJumper) {
    showQuickJumper = {
      goButton: <button className="button button-outline button-short button-rectangle">确定</button>,
    };
  }
  const align = props.align || 'center';
  const { style, ...rest } = props;
  return (
    <RcPagination
      style={{ textAlign: align, ...style }}
      {...rest}
      locale={locale}
      showQuickJumper={showQuickJumper} />
  );
};

Pagination.propTypes = {
  total: PropTypes.number.isRequired,
  pageSize: PropTypes.number,
  defaultCurrent: PropTypes.number,
  showQuickJumper: PropTypes.bool,
  showLessItems: PropTypes.bool,
  className: PropTypes.string,
  style: PropTypes.object,
  align: PropTypes.oneOf(['center', 'left', 'right']),
  current: PropTypes.number,
};
