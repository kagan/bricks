import classNames from 'classnames';
import React, { MouseEvent, ReactEventHandler } from 'react';

import { IDOMProps } from '../../utils/props';

export interface IInputProps extends IDOMProps {
  disabled?: boolean; // 是否禁用
  error?: boolean;
  className?: string;
  xSmall?: boolean;
  small?: boolean;
  large?: boolean;
  xLarge?: boolean;
  type?: string;
  defaultValue?: string;
  value?: string;
  style?: React.CSSProperties;
  icon?: JSX.Element;
  placeholder?: string;
  autoFocus?: boolean;
  maxLength?: number;
  onChange?: React.ChangeEventHandler<HTMLInputElement>;
  onFocus?: React.FocusEventHandler<HTMLInputElement>;
  onBlur?: React.FocusEventHandler<HTMLInputElement>;
  onKeyUp?: React.KeyboardEventHandler<HTMLInputElement>;
  onKeyDown?: React.KeyboardEventHandler<HTMLInputElement>;
  onClick?: React.MouseEventHandler<HTMLInputElement | HTMLDivElement>;
  number?: boolean;
}

export interface IInputState {
  value?: string;
  showError?: boolean;
}

export class Input extends React.Component<IInputProps, IInputState> {
  constructor(props: IInputProps) {
    super(props);
    this.state = {
      showError: false,
    };
    this.renderAddonAfter = this.renderAddonAfter.bind(this);
    this.handleChange = this.handleChange.bind(this);
  }

  public renderAddonAfter() {
    if (!this.props.icon) {
      return null;
    }
    return this.props.icon;
  }

  public handleChange(event: React.ChangeEvent<HTMLInputElement>) {
    if (this.props.number) {
      this.check(event.target.value);
      if (this.props.onChange && !isNaN(event.target.value as any)) {
        this.props.onChange(event);
      }
    } else {
      if (this.props.onChange) {
        this.props.onChange(event);
      }
    }
  }

  public check(val: any) {
    let showError = false;
    if (isNaN(val)) {
      // 输入的值不为数值
      showError = true;
    }
    this.setState({
      showError,
    });
  }

  public render() {
    const {
      xSmall,
      small,
      large,
      xLarge,
      disabled,
      error,
      icon,
      className,

      type,
      defaultValue,
      value,
      placeholder,
      autoFocus,
      maxLength,
      onBlur,
      onFocus,
      onChange,
      onClick,
      onKeyDown,
      onKeyUp,
      ...props } = this.props;
    const classes = {
      'br-input--xs': xSmall,
      'br-input--sm': small,
      'br-input--lg': large,
      'br-input--xl': xLarge,
      'br-input--disabled': disabled,
      'br-input--error': error,
    };

    // TODO: 通过class控制大小
    let style = {};
    if (icon) {
      style = {
        padding: '0 34px 0 10px',
      };
    }

    const inputProps = {
      type,
      defaultValue,
      value,
      placeholder,
      autoFocus,
      maxLength,
      onBlur,
      onFocus,
      onChange,
      onClick,
      onKeyDown,
      onKeyUp,
    };

    return (
      <div
        className={classNames('br-input', classes, className, {
          'br-input--error': this.state.showError,
        })}
        style={this.props.style}
      >
        <input
          className="br-input__input"
          {...inputProps}
          {...props}
          style={style}
          onChange={this.handleChange}
        />
        {this.renderAddonAfter()}
      </div>
    );
  }
}
