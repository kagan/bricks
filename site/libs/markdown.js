import React from 'react';
import ReactDOM from 'react-dom';
import marked from 'marked';
// 语法高亮
import prism from 'prismjs';
import 'prismjs/components/prism-jsx';
import 'prismjs/components/prism-tsx';
import 'prismjs/themes/prism.css'

import CodeEngine from './code-engine';

export default class Markdown extends React.Component {
  constructor(props) {
    super(props);

    this.components = new Map;

    this.renderer = new marked.Renderer();
    this.renderer.table = (header, body) => {
      return `<table class="grid"><thead>${header}</thead><tbody>${body}</tbody></table>`;
    };
    this.renderer.list = (body, ordered, start) => {
      const tag = ordered ? 'ol' : 'ul';
      return `<${tag} class="docs-list">${body}</${tag}>`
    };

    this.renderer.heading = (text, level, rawText) => {
      const classes = ['', 'title-lg', 'title', 'title-sm'];
      const cls = classes[level] || ''
      return `<h${level} style="margin: 10px 0" class="${cls} text-bold">${text}</h${level}>`;
    }
  }

  componentDidMount() {
    this.renderDOM();
  }

  componentDidUpdate() {
    this.renderDOM();
  }

  componentWillUnmount() {
    this.unmountComponent();
  }

  unmountComponent() {
    for (const [id] of this.components) {
      const div = document.getElementById(id);

      if (div instanceof HTMLElement) {
        ReactDOM.unmountComponentAtNode(div);
      }
    }
  }

  renderDOM() {
    this.unmountComponent();
    for (const [id, component] of this.components) {
      const div = document.getElementById(id);

      if (div instanceof HTMLElement) {
        ReactDOM.render(component, div);
      }
    }
    prism.highlightAll();
  }

  render() {
    const document = this.document(localStorage.getItem('ELEMENT_LANGUAGE') || 'zh-CN');

    if (typeof document === 'string') {
      this.components.clear();

      const html = marked(document.replace(/:::\s?demo\s?([^]+?):::/g, (match, p1, offset) => {
        const id = offset.toString(36);

        this.components.set(id, React.createElement(CodeEngine, Object.assign({
          // name: this.constructor.name.toLowerCase()
        }, this.props), p1));

        return `<div id=${id}></div>`;
      }), { renderer: this.renderer });

      return (
        <div dangerouslySetInnerHTML={{
          __html: html
        }} />
      )
    } else {
      return <span />
    }
  }
}
