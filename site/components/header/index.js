import React from 'react';
import './styles.scss';

export default class Header extends React.Component {
  render() {
    return (
      <header className="header-container">
        <div className="header-title">
          <span className="letter" data-letter="b">b</span>
          <span className="letter" data-letter="r">r</span>
          <span className="letter" data-letter="i">i</span>
          <span className="letter" data-letter="c">c</span>
          <span className="letter" data-letter="k">k</span>
          <span className="letter" data-letter="s">s</span>
        </div>
      </header>
    );
  }
}
